cat $1 | docker run -i --rm \
  --network dra-notaservice \
  bitnami/kafka:latest bash -c 'cat | kafka-console-producer.sh \
  --bootstrap-server kafka:9092 --topic nota-pps \
  --property parse.key=true \
  --property key.separator="|"'
