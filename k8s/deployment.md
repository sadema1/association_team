# Deploy TeamService

## Prepare
```
kubectl config get-contexts
kubectl config use-context docker-desktop
```

```
kubectl get namespaces
kubectl create namespace teamservice
// make sticky
kubectl config set-context --current --namespace=teamservice
```

## Install mariadb
```
helm install teamservice-mariadb bitnami/mariadb --namespace teamservice --set img.tag=10.9, auth.rootPassword=root, auth.username=team, auth.password=team, auth.database=team

// run client
kubectl run teamservice-mariadb-client --rm --tty -i --restart='Never' --image  docker.io/bitnami/mariadb:10.6.11-debian-11-r12 --namespace teamservice --command -- bash
mysql -h teamservice-mariadb.teamservice.svc.cluster.local -uteam -p team

// port forwarding
kubectl port-forward service/teamservice-mariadb 3306:3306 &
```

```
kubectl get secrets --all-namespaces --selector owner=helm
kubectl --namespace association describe secret sh.helm.release.v1.teamservice-mariadb.v2
```

## Install mongodb


## Apply configurations