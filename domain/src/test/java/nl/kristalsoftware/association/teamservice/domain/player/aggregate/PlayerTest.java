package nl.kristalsoftware.association.teamservice.domain.player.aggregate;

import nl.kristalsoftware.association.teamservice.domain.player.PlayerRepository;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerBirthDate;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerName;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class PlayerTest {

    private PlayerReference playerReference = PlayerReference.of(UUID.randomUUID());
    private final PlayerName playerName = PlayerName.of("Niek", "Schokman");
    private final PlayerBirthDate playerBirthDate = PlayerBirthDate.of(LocalDate.of(2004, 6, 14));
    private final PlayerKind playerKind = PlayerKind.of(PlayerKind.Kind.PLAYER);
    private final PlayerRole playerRole = PlayerRole.of(PlayerRole.Role.UNKNOWN);

    private Player playerNiekSchokman;
    private Player nonExistingPlayer;

    @BeforeEach
    void setUp() {
        playerNiekSchokman = Player.of(playerReference, true);
        playerNiekSchokman.load(PlayerSignedUp.of(
                playerReference,
                playerName,
                playerBirthDate,
                playerKind,
                playerRole
        ));
        nonExistingPlayer = Player.of(playerReference, false);
        nonExistingPlayer.load(PlayerSignedUp.of(
                playerReference,
                playerName,
                playerBirthDate,
                PlayerKind.of(PlayerKind.Kind.SUPPORTING_MEMBER),
                playerRole
        ));
    }

    @Test
    void handleMemberKindChangedEventToPlayer() {
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = nonExistingPlayer.handleMemberKindChangedEvent(
                playerName,
                playerBirthDate,
                PlayerKind.of(PlayerKind.Kind.PLAYER)
        );
        assertThat(createdDomainEvents.size()).isEqualTo(1);
        assertThat(createdDomainEvents.get(0) instanceof PlayerSignedUp).isTrue();
        PlayerSignedUp domainEvent = (PlayerSignedUp) createdDomainEvents.get(0);
        assertThat(domainEvent.getPlayerReference()).isEqualTo(playerReference);
        assertThat(domainEvent.getPlayerKind().getValue()).isEqualTo(PlayerKind.Kind.PLAYER);
    }

    @Test
    void handleMemberKindChangedEventToSupportingMember() {
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = playerNiekSchokman.handleMemberKindChangedEvent(
                playerName,
                playerBirthDate,
                PlayerKind.of(PlayerKind.Kind.SUPPORTING_MEMBER)
        );
        assertThat(createdDomainEvents.size()).isEqualTo(1);
        assertThat(createdDomainEvents.get(0) instanceof PlayerStoppedPlaying).isTrue();
        PlayerStoppedPlaying domainEvent = (PlayerStoppedPlaying) createdDomainEvents.get(0);
        assertThat(domainEvent.getPlayerReference()).isEqualTo(playerReference);
        assertThat(domainEvent.getPlayerKind().getValue()).isEqualTo(PlayerKind.Kind.SUPPORTING_MEMBER);
    }

}
