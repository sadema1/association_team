package nl.kristalsoftware.association.teamservice.domain.member.attributes;

import lombok.Getter;
import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class MemberName extends TinyStringType {

    @Getter
    final private String firstName;
    @Getter
    final private String lastName;

    private MemberName(String firstName, String lastName) {
        super(firstName + " " + lastName);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static MemberName of(String firstName, String lastName) {
        return new MemberName(firstName, lastName);
    }

}
