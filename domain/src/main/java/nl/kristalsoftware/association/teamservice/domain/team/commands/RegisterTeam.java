package nl.kristalsoftware.association.teamservice.domain.team.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommand;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.List;

@AllArgsConstructor(staticName = "of")
public class RegisterTeam implements BaseCommand<TeamRepository, Team> {

    @Getter
    private TeamName teamName;
    @Getter
    private TeamCategory teamCategory;
    @Getter
    private TeamDescription teamDescription;

    @Override
    public List<DomainEventSaving<TeamRepository>> handleCommand(Team team) {
        return team.handleCommand(this);
    }
}
