package nl.kristalsoftware.association.teamservice.domain.player.aggregate;

import lombok.Data;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerBirthDate;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerName;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRoleCollection;
import nl.kristalsoftware.ddd.domain.base.annotation.AggregateRoot;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEntity;

@AggregateRoot
@DomainEntity
@Data
public class PlayerDomainEntity {

    private PlayerName playerName;
    private PlayerBirthDate playerBirthDate;
    private PlayerKind playerKind;
    private PlayerRoleCollection playerRoleCollection;

    public void addRole(PlayerRole playerRole) {
        playerRoleCollection.addItem(playerRole);
    }

    public void removeRole(PlayerRole playerRole) {
        playerRoleCollection.removeItem(playerRole);
    }

    public boolean isPlayer() {
        return playerKind != null && playerKind.getValue().equals(PlayerKind.Kind.PLAYER);
    }
}
