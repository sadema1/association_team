package nl.kristalsoftware.association.teamservice.domain.player.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

import java.util.UUID;

@ValueObject
public class PlayerReference extends TinyUUIDType {

    private PlayerReference(UUID value) {
        super(value);
    }

    public static PlayerReference of(UUID value) {
        return new PlayerReference(value);
    }

    public static PlayerReference of(String value) {
        UUID uuid = null;
        if (value != null && !value.isEmpty() && !value.equals("0")) {
            uuid = UUID.fromString(value);
        }
        return new PlayerReference(uuid);
    }

}
