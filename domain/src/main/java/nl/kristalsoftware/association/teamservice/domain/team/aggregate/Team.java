package nl.kristalsoftware.association.teamservice.domain.team.aggregate;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamPlayersCollection;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.commands.AllocateTeamPlayers;
import nl.kristalsoftware.association.teamservice.domain.team.commands.ChangeTeamAttributes;
import nl.kristalsoftware.association.teamservice.domain.team.commands.RegisterTeam;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.ddd.domain.base.aggregate.BaseAggregateRoot;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Slf4j
public class Team extends BaseAggregateRoot<TeamReference> {

    @Getter
    private TeamDomainEntity teamDomainEntity = new TeamDomainEntity();

    private Team(TeamReference teamReference, Boolean existingAggregate) {
        super(teamReference, existingAggregate);
    }

    public static Team of(TeamReference teamReference, Boolean existingAggregate) {
        return new Team(teamReference, existingAggregate);
    }

    public TeamName getTeamName() {
        return teamDomainEntity.getTeamName();
    }

    public TeamCategory getTeamCategory() {
        return teamDomainEntity.getTeamCategory();
    }

    public TeamDescription getTeamDescription() {
        return teamDomainEntity.getTeamDescription();
    }

    public TeamPlayersCollection getTeamPlayersCollection() {
        return teamDomainEntity.getTeamPlayers();
    }

    public List<DomainEventSaving<TeamRepository>> handleCommand(RegisterTeam registerTeam) {
        List<DomainEventSaving<TeamRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(TeamRegistered.of(
                getReference(),
                registerTeam.getTeamName(),
                registerTeam.getTeamCategory(),
                registerTeam.getTeamDescription()
        ));
        return domainEventList;
    }

    public List<DomainEventSaving<TeamRepository>> handleCommand(ChangeTeamAttributes changeTeamAttributes) {
        List<DomainEventSaving<TeamRepository>> domainEventList = new ArrayList<>();
        if (changeTeamAttributes.getTeamName().isPresent() ||
                changeTeamAttributes.getTeamCategory().isPresent() ||
                changeTeamAttributes.getTeamDescription().isPresent()
        ) {
            domainEventList.add(TeamAttributesChanged.of(
                    getReference(),
                    changeTeamAttributes.getTeamName(),
                    changeTeamAttributes.getTeamCategory(),
                    changeTeamAttributes.getTeamDescription()
            ));
        }
        return domainEventList;
    }

    public List<DomainEventSaving<TeamRepository>> handleCommand(AllocateTeamPlayers allocateTeamPlayers) {
        List<DomainEventSaving<TeamRepository>> domainEventList = new ArrayList<>();
        TeamPlayersCollection teamPlayersCollection = teamDomainEntity.getTeamPlayers();
        teamPlayersCollection.setItems(allocateTeamPlayers.getPlayers());
        if (teamPlayersCollection.itemsChanged()) {
            domainEventList.add(TeamPlayersAllocated.of(getReference(), teamPlayersCollection.getAssigned(), teamPlayersCollection.getUnassigned()));
        }
        return domainEventList;
    }

    public void load(TeamRegistered teamRegistered) {
        teamDomainEntity.setTeamName(teamRegistered.getTeamName());
        teamDomainEntity.setTeamCategory(teamRegistered.getTeamCategory());
        teamDomainEntity.setTeamDescription(teamRegistered.getTeamDescription());
        teamDomainEntity.setTeamPlayers(TeamPlayersCollection.of(new HashSet<>()));
    }

    public void load(TeamAttributesChanged teamAttributesChanged) {
        teamAttributesChanged.getTeamName().ifPresent(it -> teamDomainEntity.setTeamName(it));
        teamAttributesChanged.getTeamCategory().ifPresent(it -> teamDomainEntity.setTeamCategory(it));
        teamAttributesChanged.getTeamDescription().ifPresent(it -> teamDomainEntity.setTeamDescription(it));
    }

    public void load(TeamPlayersAllocated teamPlayersAllocated) {
        for (PlayerReference playerReference : teamPlayersAllocated.getPlayersAssigned()) {
            teamDomainEntity.addPlayer(playerReference);
        }
        for (PlayerReference playerReference : teamPlayersAllocated.getPlayersUnassigned()) {
            teamDomainEntity.removePlayer(playerReference);
        }
    }
}
