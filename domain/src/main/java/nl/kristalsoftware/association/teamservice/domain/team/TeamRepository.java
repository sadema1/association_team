package nl.kristalsoftware.association.teamservice.domain.team;

import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateRepository;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainRepository;

import java.util.UUID;

@DomainRepository
public class TeamRepository extends AggregateRepository<TeamRepository, Team, TeamReference> {

    private final TeamEventProcessorPort teamEventProcessorPort;

    private final TeamViewStoreDocumentPort teamViewStorePort;

    private TeamRepository(TeamEventProcessorPort teamEventProcessorPort, TeamViewStoreDocumentPort teamViewStorePort) {
        super(exists -> Team.of(TeamReference.of(UUID.randomUUID()), exists), teamEventProcessorPort, teamViewStorePort);
        this.teamEventProcessorPort = teamEventProcessorPort;
        this.teamViewStorePort = teamViewStorePort;
    }

    private TeamRepository(TeamReference teamReference,
                           TeamEventProcessorPort teamEventProcessorPort, TeamViewStoreDocumentPort teamViewStorePort) {
        super(teamReference, t -> Team.of(teamReference, t), teamEventProcessorPort, teamViewStorePort);
        this.teamEventProcessorPort = teamEventProcessorPort;
        this.teamViewStorePort = teamViewStorePort;
    }

    public static TeamRepository of(TeamEventProcessorPort teamRepositoryPort, TeamViewStoreDocumentPort teamDocumentsRepositoryPort) {
        return new TeamRepository(teamRepositoryPort, teamDocumentsRepositoryPort);
    }

    public static TeamRepository of(TeamReference teamReference,
                                    TeamEventProcessorPort teamRepositoryPort, TeamViewStoreDocumentPort teamDocumentsRepositoryPort) {
        return new TeamRepository(teamReference, teamRepositoryPort, teamDocumentsRepositoryPort);
    }

    @Override
    protected TeamRepository getDomainRepository() {
        return this;
    }

    public void save(TeamRegistered teamRegistered) {
        teamEventProcessorPort.saveEvent(teamRegistered, teamViewStorePort);
    }

    public void save(TeamAttributesChanged teamAttributesChanged) {
        teamEventProcessorPort.saveEvent(getAggregate(), teamAttributesChanged, teamViewStorePort);
    }

    public void save(TeamPlayersAllocated teamPlayersAllocated) {
        teamEventProcessorPort.saveEvent(getAggregate(), teamPlayersAllocated, teamViewStorePort);
    }
}
