package nl.kristalsoftware.association.teamservice.domain.member.attributes;

public enum Kind {
    PLAYER, SUPPORTING_MEMBER

}
