package nl.kristalsoftware.association.teamservice.domain.player.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyDateType;

import java.time.LocalDate;

@ValueObject
public class PlayerBirthDate extends TinyDateType {

    private PlayerBirthDate(LocalDate localDate) {
        super(localDate);
    }
    private PlayerBirthDate(long dateInMillis) {
        super(TinyDateType.getLocalDateFromMillis(dateInMillis));
    }

    public static PlayerBirthDate of(LocalDate localDate) {
        return new PlayerBirthDate(localDate);
    }
    public static PlayerBirthDate of(long dateInMillis) {
        return new PlayerBirthDate(dateInMillis);
    }

}
