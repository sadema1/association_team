package nl.kristalsoftware.association.teamservice.domain.member.attributes;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyType;

@Slf4j
@ValueObject
public class MemberKind extends TinyType<Kind> {

    protected MemberKind(Kind kind) {
        super(kind);
    }

    public static MemberKind of(Kind value) {
        return new MemberKind(value);
    }

    public static MemberKind of(String value) {
        if (value != null) {
            return new MemberKind(getKind(value));
        }
        return new MemberKind(Kind.PLAYER);
    }

    private static Kind getKind(String value) {
        try {
            Kind kind = Kind.valueOf(value);
            return kind;
        }
        catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
            return Kind.PLAYER;
        }
    }

    @Override
    public Boolean isEmpty() {
        return false;
    }

}
