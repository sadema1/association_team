package nl.kristalsoftware.association.teamservice.domain.team.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class TeamDescription extends TinyStringType {

    private TeamDescription(String value) {
        super(value);
    }

    public static TeamDescription of(String value) {
        return new TeamDescription(value);
    }
    
}
