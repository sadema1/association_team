package nl.kristalsoftware.association.teamservice.domain.player.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerRepository;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommand;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.List;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class StopPlaying implements BaseCommand<PlayerRepository, Player> {

    private final PlayerKind playerKind;

    @Override
    public List<DomainEventSaving<PlayerRepository>> handleCommand(Player player) {
        return player.handleCommand(this);
    }

}
