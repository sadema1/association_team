package nl.kristalsoftware.association.teamservice.domain.player.streams;


import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;

public interface PlayerEventsStreamingPort {
    void streamEvent(PlayerSignedUp playerSignedUp);

    void streamEvent(Player player, PlayerStoppedPlaying playerStoppedPlaying);

    void streamEvent(Player player, PlayerRolesAllocated playerRolesAllocated);
}
