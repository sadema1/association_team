package nl.kristalsoftware.association.teamservice.domain.player.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyType;

@ValueObject
public class PlayerKind extends TinyType<PlayerKind.Kind> {

    public enum Kind
    {
        UNKNOWN, PLAYER, SUPPORTING_MEMBER;
    }

    protected PlayerKind(Kind value) {
        super(value);
    }

    public static PlayerKind of(String value) {
        if (value != null) {
            return new PlayerKind(getKind(value));
        }
        return new PlayerKind(Kind.UNKNOWN);
    }

    private static Kind getKind(String value) {
        Kind kind;
        try {
            kind = Kind.valueOf(value);
        }
        catch (IllegalArgumentException iae) {
            kind = Kind.UNKNOWN;
        }
        return kind;
    }

    public static PlayerKind of(Kind kind) {
        return new PlayerKind(kind);
    }

    @Override
    public Boolean isEmpty() {
        return getValue() == null || getValue().equals(Kind.UNKNOWN);
    }

    public boolean isPlayer() {
        return getValue().equals(Kind.PLAYER);
    }
}
