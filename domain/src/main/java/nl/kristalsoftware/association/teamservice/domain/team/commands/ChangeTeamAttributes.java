package nl.kristalsoftware.association.teamservice.domain.team.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommand;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor(staticName = "of")
public class ChangeTeamAttributes implements BaseCommand<TeamRepository, Team> {

    @Getter
    private Optional<TeamName> teamName;
    @Getter
    private Optional<TeamCategory> teamCategory;
    @Getter
    private Optional<TeamDescription> teamDescription;

    @Override
    public List<DomainEventSaving<TeamRepository>> handleCommand(Team team) {
        return team.handleCommand(this);
    }

}
