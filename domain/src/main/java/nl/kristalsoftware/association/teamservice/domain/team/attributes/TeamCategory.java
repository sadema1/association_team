package nl.kristalsoftware.association.teamservice.domain.team.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class TeamCategory extends TinyStringType {

    private TeamCategory(String value) {
        super(value);
    }

    public static TeamCategory of(String value) {
        return new TeamCategory(value);
    }

}
