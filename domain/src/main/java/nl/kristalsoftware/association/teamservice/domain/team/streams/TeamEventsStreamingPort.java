package nl.kristalsoftware.association.teamservice.domain.team.streams;


import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;

public interface TeamEventsStreamingPort {
    void streamEvent(TeamRegistered teamRegistered);

    void streamEvent(Team team, TeamAttributesChanged teamAttributesChanged);

    void streamEvent(Team team, TeamPlayersAllocated teamPlayersAllocated);
}
