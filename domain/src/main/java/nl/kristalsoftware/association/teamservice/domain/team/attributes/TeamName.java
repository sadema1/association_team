package nl.kristalsoftware.association.teamservice.domain.team.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class TeamName extends TinyStringType {

    private TeamName(String value) {
        super(value);
    }

    public static TeamName of(String value) {
        return new TeamName(value);
    }
}
