package nl.kristalsoftware.association.teamservice.domain.team.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEvent;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.Set;

@DomainEvent
@Getter
@RequiredArgsConstructor(staticName = "of")
public class TeamPlayersAllocated implements DomainEventSaving<TeamRepository>, DomainEventLoading<Team> {
    private final TeamReference teamReference;
    private final Set<PlayerReference> playersAssigned;
    private final Set<PlayerReference> playersUnassigned;

    @Override
    public void load(Team aggregate) {
        aggregate.load(this);
    }

    @Override
    public void save(TeamRepository repository) {
        repository.save(this);
    }
}
