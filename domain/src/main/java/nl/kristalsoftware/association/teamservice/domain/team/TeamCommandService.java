package nl.kristalsoftware.association.teamservice.domain.team;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.commands.AllocateTeamPlayers;
import nl.kristalsoftware.association.teamservice.domain.team.commands.ChangeTeamAttributes;
import nl.kristalsoftware.association.teamservice.domain.team.commands.RegisterTeam;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainService;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommandService;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service
@DomainService
public class TeamCommandService extends BaseCommandService<TeamRepository, Team> {

    private final TeamEventProcessorPort teamEventProcessingPort;
    private final ObjectProvider<TeamViewStoreDocumentPort> viewStoreDocumentProvider;

    public Optional<TeamReference> registerTeam(
            TeamName teamName,
            TeamCategory teamCategory,
            TeamDescription teamDescription) {
        TeamRepository teamRepository = TeamRepository.of(
                teamEventProcessingPort,
                viewStoreDocumentProvider.getObject()
                );
        List<DomainEventSaving<TeamRepository>> domainEventList = sendCommand(RegisterTeam.of(teamName, teamCategory, teamDescription), teamRepository.getAggregate());
        if (teamRepository.saveEvents(domainEventList)) {
            return Optional.of(teamRepository.getAggregate().getReference());
        }
        return Optional.empty();
    }

    public void changeTeamAttributes(
            TeamReference teamReference,
            TeamName teamName,
            TeamCategory teamCategory,
            TeamDescription teamDescription,
            Set<PlayerReference> players) throws AggregateNotFoundException {
        TeamRepository teamRepository = TeamRepository.of(
                teamReference,
                teamEventProcessingPort,
                viewStoreDocumentProvider.getObject()
                );
        List<DomainEventSaving<TeamRepository>> createdDomainEvents = handleChangedAttributes(teamRepository, teamName, teamCategory, teamDescription);
        createdDomainEvents.addAll(sendCommand(AllocateTeamPlayers.of(players), teamRepository.getAggregate()));
        teamRepository.saveEvents(createdDomainEvents);
    }

    private List<DomainEventSaving<TeamRepository>>  handleChangedAttributes(TeamRepository teamRepository, TeamName teamName, TeamCategory teamCategory, TeamDescription teamDescription) {
        Optional<TeamName> changedTeamName = getChangedAttribute(teamRepository.getAggregate().getTeamName(), teamName);
        Optional<TeamCategory> changedTeamCategory = getChangedAttribute(teamRepository.getAggregate().getTeamCategory(), teamCategory);
        Optional<TeamDescription> changedTeamDescription = getChangedAttribute(teamRepository.getAggregate().getTeamDescription(), teamDescription);
        return sendCommand(ChangeTeamAttributes.of(changedTeamName, changedTeamCategory, changedTeamDescription), teamRepository.getAggregate());
    }

    private <T> Optional<T> getChangedAttribute(T aggregateAttribute, T changedAttribute) {
        if (aggregateAttribute.equals(changedAttribute)) {
            return Optional.empty();
        }
        return Optional.of(changedAttribute);
    }

}
