package nl.kristalsoftware.association.teamservice.domain.player.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerRepository;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEvent;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

@DomainEvent
@Getter
@RequiredArgsConstructor(staticName = "of")
public class PlayerStoppedPlaying implements DomainEventSaving<PlayerRepository>, DomainEventLoading<Player> {
    private final PlayerReference playerReference;
    private final PlayerKind playerKind;

    @Override
    public void load(Player aggregate) {
        aggregate.load(this);
    }

    @Override
    public void save(PlayerRepository repository) {
        repository.save(this);
    }
}
