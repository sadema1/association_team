package nl.kristalsoftware.association.teamservice.domain.player.events;

import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.ddd.domain.base.event.EventStorePort;

public interface PlayerEventStorePort extends EventStorePort<Player, PlayerReference> {
    void saveEvent(PlayerSignedUp playerSignedUp);

    void saveEvent(PlayerStoppedPlaying playerStoppedPlaying);

    void saveEvent(PlayerRolesAllocated playerRolesAllocated);

}
