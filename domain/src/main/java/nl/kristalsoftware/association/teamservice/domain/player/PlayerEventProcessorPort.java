package nl.kristalsoftware.association.teamservice.domain.player;

import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateEventProcessor;

public interface PlayerEventProcessorPort extends AggregateEventProcessor<Player, PlayerReference> {
    void saveEvent(PlayerSignedUp playerSignedUp, PlayerViewStoreDocumentPort playerViewStoreDocumentPort);

    void saveEvent(Player aggregate, PlayerStoppedPlaying playerStoppedPlaying, PlayerViewStoreDocumentPort playerViewStoreDocumentPort);

    void saveEvent(Player aggregate, PlayerRolesAllocated playerRolesAllocated, PlayerViewStoreDocumentPort playerViewStoreDocumentPort);
}
