package nl.kristalsoftware.association.teamservice.domain.player.aggregate;

import lombok.Getter;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerRepository;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerBirthDate;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerName;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRoleCollection;
import nl.kristalsoftware.association.teamservice.domain.player.commands.AllocatePlayerRoles;
import nl.kristalsoftware.association.teamservice.domain.player.commands.StopPlaying;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.ddd.domain.base.aggregate.BaseAggregateRoot;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Player extends BaseAggregateRoot<PlayerReference> {

    @Getter
    private PlayerDomainEntity playerDomainEntity = new PlayerDomainEntity();

    private Player(PlayerReference reference, boolean existingAggregate) {
        super(reference, existingAggregate);
    }

    public static Player of(PlayerReference playerReference, boolean existingAggregate) {
        return new Player(playerReference, existingAggregate);
    }

    public List<DomainEventSaving<PlayerRepository>> handleMemberSignedUpEvent(PlayerName playerName, PlayerBirthDate playerBirthDate, PlayerKind playerKind) {
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = new ArrayList<>();
        if (isNonExistingAggregate() && playerKind.isPlayer()) {
            createdDomainEvents.add(
                    PlayerSignedUp.of(
                            getReference(),
                            playerName,
                            playerBirthDate,
                            playerKind,
                            PlayerRole.of(PlayerRole.Role.UNKNOWN)
                    )
            );
        }
        return createdDomainEvents;
    }

    public List<DomainEventSaving<PlayerRepository>> handleMemberKindChangedEvent(PlayerName playerName, PlayerBirthDate playerBirthDate, PlayerKind playerKind) {
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = new ArrayList<>();
        if (aggregateIsPlayer()) {
            if (isNotPlayingAnymore(playerKind)) {
                createdDomainEvents.add(
                        PlayerStoppedPlaying.of(
                                getReference(),
                                playerKind
                        )
                );
            }
        } else {
            if (isNonExistingAggregate() && willBecomePlayer(playerKind)) {
                createdDomainEvents.add(
                        PlayerSignedUp.of(
                                getReference(),
                                playerName,
                                playerBirthDate,
                                playerKind,
                                PlayerRole.of(PlayerRole.Role.UNKNOWN)
                        )
                );
            }
        }
        return createdDomainEvents;
    }

    private boolean aggregateIsPlayer() {
        return playerDomainEntity.isPlayer();
    }

    private boolean willBecomePlayer(PlayerKind playerKind) {
        return playerKind.getValue().equals(PlayerKind.Kind.PLAYER);
    }

    private boolean isNotPlayingAnymore(PlayerKind playerKind) {
        return !playerKind.getValue().equals(PlayerKind.Kind.PLAYER);
    }

    public List<DomainEventSaving<PlayerRepository>> handleCommand(StopPlaying stopPlaying) {
        List<DomainEventSaving<PlayerRepository>> domainEventList = new ArrayList<>();
        if (aggregateIsPlayer() && isNotPlayingAnymore(stopPlaying.getPlayerKind())) {
            domainEventList.add(
                    PlayerStoppedPlaying.of(
                            getReference(),
                            stopPlaying.getPlayerKind()
                    )
            );
        }
        return domainEventList;
    }

    public List<DomainEventSaving<PlayerRepository>> handleCommand(AllocatePlayerRoles allocatePlayerRoles) {
        List<DomainEventSaving<PlayerRepository>> domainEventList = new ArrayList<>();
        PlayerRoleCollection playerRoleCollection = playerDomainEntity.getPlayerRoleCollection();
        playerRoleCollection.setItems(allocatePlayerRoles.getPlayerRoles());
        if (playerRoleCollection.itemsChanged()) {
            domainEventList.add(PlayerRolesAllocated.of(getReference(), playerRoleCollection.getAssigned(), playerRoleCollection.getUnassigned()));
        }
        return domainEventList;
    }

    public void load(PlayerSignedUp playerSignedUp) {
        playerDomainEntity.setPlayerName(playerSignedUp.getPlayerName());
        playerDomainEntity.setPlayerBirthDate(playerSignedUp.getPlayerBirthDate());
        playerDomainEntity.setPlayerKind(playerSignedUp.getPlayerKind());
        playerDomainEntity.setPlayerRoleCollection(PlayerRoleCollection.of(new HashSet<>()));
    }

    public void load(PlayerStoppedPlaying playerStoppedPlaying) {
        playerDomainEntity.setPlayerKind(playerStoppedPlaying.getPlayerKind());
    }

    public void load(PlayerRolesAllocated playerRolesAllocated) {
        for (PlayerRole playerRole : playerRolesAllocated.getPlayerRolesAssigned()) {
            playerDomainEntity.addRole(playerRole);
        }
        for (PlayerRole playerRole : playerRolesAllocated.getPlayerRolesUnassigned()) {
            playerDomainEntity.removeRole(playerRole);
        }
    }

}
