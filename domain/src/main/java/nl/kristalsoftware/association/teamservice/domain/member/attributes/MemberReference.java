package nl.kristalsoftware.association.teamservice.domain.member.attributes;


import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

import java.util.UUID;

@ValueObject
public class MemberReference extends TinyUUIDType {

    private MemberReference(UUID value) {
        super(value);
    }

    public static MemberReference of(UUID value) {
        return new MemberReference(value);
    }

    public static MemberReference of(String value) {
        UUID uuid = null;
        if (value != null && !value.isEmpty() && !value.equals("0")) {
            uuid = UUID.fromString(value);
        }
        return new MemberReference(uuid);
    }

}
