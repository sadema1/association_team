package nl.kristalsoftware.association.teamservice.domain.player;

import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateRepository;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainRepository;

@DomainRepository
public class PlayerRepository extends AggregateRepository<PlayerRepository, Player, PlayerReference> {

    private final PlayerEventProcessorPort playerEventProcessorPort;

    private final PlayerViewStoreDocumentPort playerViewStoreDocumentPort;

    private PlayerRepository(PlayerReference playerReference, PlayerEventProcessorPort playerEventProcessorPort, PlayerViewStoreDocumentPort playerViewStoreDocumentPort) {
        super(playerReference, exists -> Player.of(playerReference, exists), playerEventProcessorPort, playerViewStoreDocumentPort);
        this.playerEventProcessorPort = playerEventProcessorPort;
        this.playerViewStoreDocumentPort = playerViewStoreDocumentPort;
    }

    public static PlayerRepository of(PlayerReference reference,
                                      PlayerEventProcessorPort playerEventProcessorPort, PlayerViewStoreDocumentPort playerViewStoreDocumentPort) {
        return new PlayerRepository(reference, playerEventProcessorPort, playerViewStoreDocumentPort);
    }

    @Override
    protected PlayerRepository getDomainRepository() {
        return this;
    }

    public void save(PlayerSignedUp playerSignedUp) {
        playerEventProcessorPort.saveEvent(playerSignedUp, playerViewStoreDocumentPort);
    }

    public void save(PlayerStoppedPlaying playerStoppedPlaying) {
        playerEventProcessorPort.saveEvent(getAggregate(), playerStoppedPlaying, playerViewStoreDocumentPort);
    }

    public void save(PlayerRolesAllocated playerRolesAllocated) {
        playerEventProcessorPort.saveEvent(getAggregate(), playerRolesAllocated, playerViewStoreDocumentPort);
    }

}
