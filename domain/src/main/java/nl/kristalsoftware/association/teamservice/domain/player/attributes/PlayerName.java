package nl.kristalsoftware.association.teamservice.domain.player.attributes;

import lombok.Getter;
import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class PlayerName extends TinyStringType {

    @Getter
    private final String firstName;
    @Getter
    private final String lastName;

    private PlayerName(String firstName, String lastName) {
        super(firstName + " " + lastName);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static PlayerName of(String firstName, String lastName) {
        return new PlayerName(firstName, lastName);
    }

}
