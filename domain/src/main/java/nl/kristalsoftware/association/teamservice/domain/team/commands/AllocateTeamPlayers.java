package nl.kristalsoftware.association.teamservice.domain.team.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommand;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.List;
import java.util.Set;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class AllocateTeamPlayers implements BaseCommand<TeamRepository, Team> {

    private final Set<PlayerReference> players;

    @Override
    public List<DomainEventSaving<TeamRepository>> handleCommand(Team team) {
        return team.handleCommand(this);
    }

}
