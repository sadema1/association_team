package nl.kristalsoftware.association.teamservice.domain.player;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerBirthDate;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerName;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.commands.AllocatePlayerRoles;
import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainService;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommandService;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service
@DomainService
public class PlayerCommandService extends BaseCommandService<PlayerRepository, Player> {

    private final PlayerEventProcessorPort playerEventProcessorPort;
    private final ObjectProvider<PlayerViewStoreDocumentPort> viewStoreDocumentProvider;

    public void handleMemberSignedUpEvent(
            String memberReference,
            String firstName,
            String lastName,
            long memberBirthDate,
            String memberKind) {
        PlayerReference playerReference = PlayerReference.of(memberReference);
        PlayerRepository playerRepository = PlayerRepository.of(
                playerReference,
                playerEventProcessorPort,
                viewStoreDocumentProvider.getObject());
        Player player = playerRepository.getAggregate();
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = player.handleMemberSignedUpEvent(
                PlayerName.of(firstName, lastName),
                PlayerBirthDate.of(memberBirthDate),
                PlayerKind.of(memberKind)
        );
        playerRepository.saveEvents(createdDomainEvents);
    }

    public void handleMemberKindChanged(
            String memberReference,
            String firstName,
            String lastName,
            long memberBirthDate,
            String memberKind) {
        log.info(memberReference, firstName, lastName, memberBirthDate, memberKind);
        PlayerReference playerReference = PlayerReference.of(memberReference);
        PlayerRepository playerRepository = PlayerRepository.of(
                playerReference,
                playerEventProcessorPort,
                viewStoreDocumentProvider.getObject());
        Player player = playerRepository.getAggregate();
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = player.handleMemberKindChangedEvent(
                PlayerName.of(firstName, lastName),
                PlayerBirthDate.of(memberBirthDate),
                PlayerKind.of(memberKind)

        );
        playerRepository.saveEvents(createdDomainEvents);
    }

    public void changePlayerAttributes(
            PlayerReference playerReference,
            Set<PlayerRole> playerRoles) throws AggregateNotFoundException {
        PlayerRepository playerRepository = PlayerRepository.of(
                playerReference,
                playerEventProcessorPort,
                viewStoreDocumentProvider.getObject());
        List<DomainEventSaving<PlayerRepository>> createdDomainEvents = sendCommand(AllocatePlayerRoles.of(playerRoles), playerRepository.getAggregate());
        playerRepository.saveEvents(createdDomainEvents);
    }

}
