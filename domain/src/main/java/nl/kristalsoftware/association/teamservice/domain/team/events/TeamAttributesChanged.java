package nl.kristalsoftware.association.teamservice.domain.team.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEvent;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.Optional;

@DomainEvent
@Slf4j
@Getter
@RequiredArgsConstructor(staticName = "of")
public class TeamAttributesChanged implements DomainEventLoading<Team>, DomainEventSaving<TeamRepository> {

    private final TeamReference teamReference;
    private final Optional<TeamName> teamName;
    private final Optional<TeamCategory> teamCategory;
    private final Optional<TeamDescription> teamDescription;

    @Override
    public void load(Team aggregate) {
        aggregate.load(this);
    }

    @Override
    public void save(TeamRepository teamRepository) {
        teamRepository.save(this);
    }
}
