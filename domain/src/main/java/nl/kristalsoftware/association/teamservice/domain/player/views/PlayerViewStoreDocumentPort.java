package nl.kristalsoftware.association.teamservice.domain.player.views;

import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.ddd.domain.base.view.ViewStoreDocumentPort;

public interface PlayerViewStoreDocumentPort extends ViewStoreDocumentPort<Player> {
    void saveEvent(PlayerSignedUp playerSignedUp);

    void saveEvent(PlayerStoppedPlaying playerStoppedPlaying);

    void saveEvent(PlayerRolesAllocated playerRolesAllocated);

}
