package nl.kristalsoftware.association.teamservice.domain.team.events;

import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainRepository;
import nl.kristalsoftware.ddd.domain.base.event.EventStorePort;

@DomainRepository
public interface TeamEventStorePort extends EventStorePort<Team, TeamReference> {
    void saveEvent(TeamRegistered teamRegistered);

    void saveEvent(TeamAttributesChanged teamAttributesChanged);

    void saveEvent(TeamPlayersAllocated teamPlayersAllocated);
}
