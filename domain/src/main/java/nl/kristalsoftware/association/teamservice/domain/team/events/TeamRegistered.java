package nl.kristalsoftware.association.teamservice.domain.team.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEvent;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

@DomainEvent
@Getter
@RequiredArgsConstructor(staticName = "of")
public class TeamRegistered implements DomainEventLoading<Team>, DomainEventSaving<TeamRepository> {

    private final TeamReference teamReference;
    private final TeamName teamName;
    private final TeamCategory teamCategory;
    private final TeamDescription teamDescription;

    @Override
    public void load(Team aggregate) {
        aggregate.load(this);
    }

    @Override
    public void save(TeamRepository repository) {
        repository.save(this);
    }
}
