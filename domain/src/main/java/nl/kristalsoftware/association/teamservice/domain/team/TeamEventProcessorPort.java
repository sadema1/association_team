package nl.kristalsoftware.association.teamservice.domain.team;

import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateEventProcessor;

public interface TeamEventProcessorPort extends AggregateEventProcessor<Team, TeamReference> {
    void saveEvent(TeamRegistered teamRegistered, TeamViewStoreDocumentPort teamViewStorePort);

    void saveEvent(Team aggregate, TeamAttributesChanged teamAttributesChanged, TeamViewStoreDocumentPort teamViewStorePort);

    void saveEvent(Team aggregate, TeamPlayersAllocated teamPlayersAllocated, TeamViewStoreDocumentPort teamViewStorePort);

}
