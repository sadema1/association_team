package nl.kristalsoftware.association.teamservice.domain.player.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinySetType;

import java.util.Set;

@ValueObject
public class PlayerRoleCollection extends TinySetType<PlayerRole> {

    private PlayerRoleCollection(Set<PlayerRole> playerRoles) {
        super(playerRoles);
    }

    public static PlayerRoleCollection of(Set<PlayerRole> playerRoles) {
        return new PlayerRoleCollection(playerRoles);
    }

}
