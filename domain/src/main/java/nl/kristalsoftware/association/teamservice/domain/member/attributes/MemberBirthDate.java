package nl.kristalsoftware.association.teamservice.domain.member.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyDateType;

import java.time.Instant;
import java.time.LocalDate;

@ValueObject
public class MemberBirthDate extends TinyDateType {

    private MemberBirthDate(LocalDate localDate) {
        super(localDate);
    }

    public static MemberBirthDate of(LocalDate localDate) {
        return new MemberBirthDate(localDate);
    }

    public static MemberBirthDate of(Long dateInMillis) {
        return new MemberBirthDate(TinyDateType.getLocalDateFromMillis(dateInMillis));
    }

    public static MemberBirthDate of(Instant instant) {
        return new MemberBirthDate(getLocalDateFromInstant(instant));
    }
}
