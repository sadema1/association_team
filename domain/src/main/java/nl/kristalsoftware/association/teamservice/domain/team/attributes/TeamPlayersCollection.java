package nl.kristalsoftware.association.teamservice.domain.team.attributes;

import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinySetType;

import java.util.Set;

@ValueObject
public class TeamPlayersCollection extends TinySetType<PlayerReference> {

    private TeamPlayersCollection(Set<PlayerReference> players) {
        super(players);
    }

    public static TeamPlayersCollection of(Set<PlayerReference> teams) {
        return new TeamPlayersCollection(teams);
    }

}
