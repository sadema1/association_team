package nl.kristalsoftware.association.teamservice.domain.team.attributes;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

import java.util.UUID;

@ValueObject
public class TeamReference extends TinyUUIDType {

    private TeamReference(UUID value) {
        super(value);
    }

    public static TeamReference of(UUID value) {
        return new TeamReference(value);
    }

    public static TeamReference of(String value) {
        UUID uuid = null;
        if (value != null && !value.isEmpty() && !value.equals("0")) {
            uuid = UUID.fromString(value);
        }
        return new TeamReference(uuid);
    }

}

