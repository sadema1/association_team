package nl.kristalsoftware.association.teamservice.domain.team.views;

import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainRepository;
import nl.kristalsoftware.ddd.domain.base.view.ViewStoreDocumentPort;

@DomainRepository
public interface TeamViewStoreDocumentPort extends ViewStoreDocumentPort<Team> {
    void saveEvent(TeamRegistered teamRegistered);

    void saveEvent(TeamAttributesChanged teamAttributesChanged);

    void saveEvent(TeamPlayersAllocated teamPlayersAllocated);
}
