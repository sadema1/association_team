package nl.kristalsoftware.association.teamservice.domain.team.aggregate;

import lombok.Data;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamPlayersCollection;
import nl.kristalsoftware.ddd.domain.base.annotation.AggregateRoot;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEntity;

@AggregateRoot
@DomainEntity
@Data
public class TeamDomainEntity {

    private TeamName teamName;
    private TeamCategory teamCategory;
    private TeamDescription teamDescription;
    private TeamPlayersCollection teamPlayers;

    public void addPlayer(PlayerReference player) {
        teamPlayers.addItem(player);
    }

    public void removePlayer(PlayerReference player) {
        teamPlayers.removeItem(player);
    }

}
