FROM eclipse-temurin:17-jdk-alpine as build
LABEL authors="sjoerdadema"

RUN apk add protoc && apk add git && ln -s /usr/bin/protoc /usr/local/bin/protoc

RUN mkdir -p /usr/src
WORKDIR /usr/src
RUN git clone https://gitlab.com/sadema1/ddd-event-frame.git
RUN cd ddd-event-frame && ls -la && ./mvnw clean install && cd -

RUN git clone https://gitlab.com/sadema1/association_team.git
RUN cd association_team && ./mvnw -DskipTests clean package

FROM eclipse-temurin:17-jdk-alpine
LABEL authors="sjoerdadema"

RUN mkdir -p /usr/src
WORKDIR /usr/src
COPY --from=build /usr/src/association_team/teamservice/target/teamservice-*.jar ./

ENV SPRING_DATASOURCE_URL=jdbc:mariadb://host.docker.internal:9806/team
ENV SPRING_DATA_MONGODB_HOST=host.docker.internal
ENV SPRING_KAFKA_BOOTSTRAP_SERVERS=host.docker.internal:29092
CMD ["java", "-jar", "teamservice-3.0.0.jar"]
