# Association TeamService

## Purpose
The purpose of this service is team management. 
Allocate players to teams. This service create the players based on events from the MemberService.
There is a REST API for creating teams and for allocating players to teams.
This service has been tested on a MacBook Pro Intel with Docker Desktop but should also work on Linux and Windows

## Prerequisites
A Docker environment is needed to run the software. 
Before running this service Kafka must have been started

### docker compose file for Kafka
```shell
version: "2"

services:
  zookeeper:
    image: 'confluentinc/cp-zookeeper:7.2.1'
    ports:
      - '2181:2181'
    environment:
      - ZOOKEEPER_CLIENT_PORT=2181

  kafka:
    image: 'confluentinc/cp-kafka:7.2.1'
    ports:
      - '9092:9092'
      - '29092:29092'
    environment:
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181
      - KAFKA_LISTENERS=LISTENER_LOCALHOST://0.0.0.0:9092,LISTENER_DOCKER://0.0.0.0:29092
      - KAFKA_ADVERTISED_LISTENERS=LISTENER_LOCALHOST://localhost:9092,LISTENER_DOCKER://host.docker.internal:29092
      - KAFKA_LISTENER_SECURITY_PROTOCOL_MAP=LISTENER_LOCALHOST:PLAINTEXT,LISTENER_DOCKER:PLAINTEXT
      - KAFKA_INTER_BROKER_LISTENER_NAME=LISTENER_LOCALHOST
      - KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1
    depends_on:
      - zookeeper
```

```shell
// start kafka
docker compose up -d

// stop kafka
docker compose down -v
```

## Run on Docker without a local Java environment

### Build the software
Clone this repo or just grab the Dockerfile.
There is no Java environment needed to build and run the software.

```shell
docker build --no-cache -t teamservice:1.0 .
```

## Run the software
The TeamService needs a mariadb and a mongo database. These can be started with the provided docker-compose.yml file.

```shell
version: "2.1"

services:
  mariadb:
    image: docker.io/bitnami/mariadb:10.9
    ports:
      - '9806:3306'
    volumes:
      - 'mariadb_data:/bitnami/mariadb'
    environment:
      - ALLOW_EMPTY_PASSWORD=yes
      - MARIADB_ROOT_PASSWORD=team
      - MARIADB_USER=team
      - MARIADB_PASSWORD=team
      - MARIADB_DATABASE=team
      - MARIADB_SKIP_TEST_DB=yes
    healthcheck:
      test: ['CMD', '/opt/bitnami/scripts/mariadb/healthcheck.sh']
      interval: 15s
      timeout: 5s
      retries: 6

  mongodb:
    image: docker.io/bitnami/mongodb:6.0
    ports:
      - "29806:27017"
    volumes:
      - 'mongodb_data:/bitnami/mongodb'
    environment:
      - MONGODB_ROOT_PASSWORD=welkom123
      - MONGODB_USERNAME=team
      - MONGODB_PASSWORD=team
      - MONGODB_DATABASE=team

volumes:
  mariadb_data:
    driver: local
  mongodb_data:
    driver: local
```

### Start the databases

```shell
// start the databases
docker compose up -d

// stop the databases
docker compose down -v
```

### Start the Java microservice

```shell
docker run -d --rm -name TeamService -p 9876:9876 teamservice:1.0
```

## Run locally within a Java environment
You can run this microservice directly from a local system with Java 17 installed

### Build the software
First you have to build the ddd-event-frame jar. The repository url: [https://gitlab.com/sadema1/ddd-event-frame](https://gitlab.com/sadema1/ddd-event-frame) 
```shell
./mvnw clean install
```

## Swagger UI

```shell
http://localhost:9876/swagger-ui/index.html
```
