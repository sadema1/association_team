create database if not exists team;

use team;

drop sequence if exists topicpartition_seq;
drop table if exists topicpartition;
drop table if exists team_registered_event;
drop table if exists team_attributes_changed_event;
drop sequence if exists team_base_event_entity_seq;
drop table if exists team_base_event_entity;
drop table if exists player_signed_up_event;
drop table if exists player_stopped_playing_event;
drop table if exists player_assigned_to_team_event;
drop table if exists player_roles_event;
drop sequence if exists player_role_assigned_event_entity_seq;
drop table if exists player_role_assigned_event;
drop sequence if exists player_role_unassigned_event_entity_seq;
drop table if exists player_role_unassigned_event;
drop table if exists team_players_event;
drop sequence if exists player_assigned_to_team_seq;
drop table if exists player_assigned_to_team;
drop sequence if exists player_unassigned_to_team_seq;
drop table if exists player_unassigned_to_team;
drop sequence if exists player_base_event_entity_seq;
drop table if exists player_base_event_entity;

-- CREATE SEQUENCE hibernate_sequence INCREMENT 1 MINVALUE 1 MAXVALUE 922337203685477580 START 1 CACHE 1;

create sequence topicpartition_seq increment 1;
create table topicpartition
(
    id               bigint       not null primary key,
    partition_offset bigint       not null,
    partition_number int          not null,
    topic_name       varchar(255) not null
    );

-- CREATE SEQUENCE team_base_event_entity_seq INCREMENT 1 MINVALUE 1 MAXVALUE 922337203685477580 START 1 CACHE 1;

create sequence team_base_event_entity_seq increment 1;
create table if not exists team_base_event_entity
(
    id                 bigint       not null primary key,
    creation_date_time datetime(6)  not null,
    domain_event_name  varchar(255) not null,
    reference          varchar(255) not null
    );

create table if not exists team_registered_event
(
    name        varchar(255)    not null,
    category    varchar(255)    null,
    description varchar(255)    null,
    id          bigint          not null primary key,
    constraint foreign key (id) references team_base_event_entity (id)
    );

create table if not exists team_attributes_changed_event
(
    name        varchar(255)    null,
    category    varchar(255)    null,
    description varchar(255)    null,
    id          bigint          not null primary key,
    constraint foreign key (id) references team_base_event_entity (id)
    );

create sequence player_base_event_entity_seq increment 1;
create table if not exists player_base_event_entity
(
    id                 bigint       not null primary key,
    creation_date_time datetime(6)  not null,
    domain_event_name  varchar(255) not null,
    reference          varchar(255) not null
    );

create table if not exists player_signed_up_event
(
    first_name  varchar(255)    not null,
    last_name   varchar(255)    not null,
    birth_date  date            null,
    kind        varchar(255)    not null,
    id          bigint          not null primary key,
    constraint foreign key (id) references player_base_event_entity (id)
    );

create table if not exists player_stopped_playing_event
(
    kind        varchar(255)    not null,
    id          bigint          not null primary key,
    constraint foreign key (id) references player_base_event_entity (id)
);

create table if not exists player_roles_event
(
    id          bigint          not null primary key,
    constraint foreign key (id) references player_base_event_entity (id)
);

create sequence player_role_assigned_event_entity_seq increment 1;
create table if not exists player_role_assigned_event
(
    id          bigint          not null primary key,
    role        varchar(255)    not null,
    player_id   bigint          not null,
    constraint foreign key (player_id) references player_roles_event (id)
);

create sequence player_role_unassigned_event_entity_seq increment 1;
create table if not exists player_role_unassigned_event
(
    id          bigint          not null primary key,
    role        varchar(255)    not null,
    player_id   bigint          not null,
    constraint foreign key (player_id) references player_roles_event (id)
);

create table if not exists team_players_event
(
    id          bigint          not null primary key,
    constraint foreign key (id) references team_base_event_entity (id)
);

create sequence player_assigned_to_team_seq increment 1;
create table if not exists player_assigned_to_team
(
    id          bigint          not null primary key,
    player_reference  varchar(255)    not null,
    team_id   bigint          not null,
    constraint foreign key (team_id) references team_players_event (id)
);

create sequence player_unassigned_to_team_seq increment 1;
create table if not exists player_unassigned_to_team
(
    id          bigint          not null primary key,
    player_reference  varchar(255)    not null,
    team_id   bigint          not null,
    constraint foreign key (team_id) references team_players_event (id)
);

