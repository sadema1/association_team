package nl.kristalsoftware.association.teamservice.player.viewstore;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@Document
public class PlayerDocument {
    @Setter(AccessLevel.NONE)
    @Id
    private ObjectId _id;
    private UUID reference;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String kind;
    private Set<String> roles = new LinkedHashSet<>();
    private Set<UUID> teamReferences = new LinkedHashSet<>();
    @Version
    private Long version;

    private PlayerDocument(UUID reference, String firstName, String lastName, LocalDate birthDate, String kind, Set<String> roles, Set<UUID> teamReferences) {
        this.reference = reference;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.kind = kind;
        this.roles = roles;
        this.teamReferences = teamReferences;
    }

    public static PlayerDocument of(UUID reference, String firstName, String lastName, LocalDate playerBirthDate, String kind, Set<String> roles, Set<UUID> teams) {
        return new PlayerDocument(reference, firstName, lastName, playerBirthDate, kind, roles, teams);
    }

    public void assignPlayerRoles(Set<String> playerRolesAsString) {
        roles.addAll(playerRolesAsString);
    }

    public void unassignPlayerRoles(Set<String> playerRolesAsString) {
        roles.removeAll(playerRolesAsString);
    }

}
