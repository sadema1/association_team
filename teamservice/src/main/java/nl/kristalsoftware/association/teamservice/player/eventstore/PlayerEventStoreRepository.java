package nl.kristalsoftware.association.teamservice.player.eventstore;


import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface PlayerEventStoreRepository extends CrudRepository<PlayerBaseEventEntity, Long> {

    Iterable<PlayerBaseEventEntity<? extends DomainEventLoading<Player>>> findAllByReference(UUID value);

    Optional<PlayerBaseEventEntity<?>> findFirstByReference(UUID value);

}
