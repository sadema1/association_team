package nl.kristalsoftware.association.teamservice.player.viewstore.rest.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PlayerChangeAttributesRequestBody {
    private Set<String> roles;
    private Set<UUID> teams;
}
