package nl.kristalsoftware.association.teamservice.player.viewstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PlayerViewStoreDocumentAdapter implements PlayerViewStoreDocumentPort {

    private final PlayerDocumentRepository playerDocumentsRepository;

    private PlayerDocument playerDocument = new PlayerDocument();

    @Override
    public void createDocument(Player aggregate) {
        playerDocument = playerDocumentsRepository.findByReference(aggregate.getReference().getValue())
                .orElseThrow(() -> new IllegalStateException(String.format("Document with reference %s not found in viewstore!", aggregate.getReference().getValue())));
     }

    @Override
    public void saveEvent(PlayerSignedUp playerSignedUp) {
        playerDocument = PlayerDocument.of(
                playerSignedUp.getPlayerReference().getValue(),
                playerSignedUp.getPlayerName().getFirstName(),
                playerSignedUp.getPlayerName().getLastName(),
                playerSignedUp.getPlayerBirthDate().getValue(),
                playerSignedUp.getPlayerKind().getValue().name(),
                new HashSet<>(),
                new HashSet<>()
        );
    }

    @Override
    public void saveEvent(PlayerStoppedPlaying playerStoppedPlaying) {
        playerDocument.setKind(playerStoppedPlaying.getPlayerKind().getValue().name());
    }

    @Override
    public void saveEvent(PlayerRolesAllocated playerRolesAllocated) {
        Set<String> newAssignedPlayerRolesAsString = playerRolesAllocated.getPlayerRolesAssigned().stream()
                .map(it -> it.getValue().name())
                .collect(Collectors.toSet());
        playerDocument.assignPlayerRoles(newAssignedPlayerRolesAsString);
        Set<String> newUnassignedPlayerRolesAsString = playerRolesAllocated.getPlayerRolesUnassigned().stream()
                .map(it -> it.getValue().name())
                .collect(Collectors.toSet());
        playerDocument.unassignPlayerRoles(newUnassignedPlayerRolesAsString);
    }

    @Override
    public void saveDocument() {
        playerDocumentsRepository.save(playerDocument);
    }
}
