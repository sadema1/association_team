package nl.kristalsoftware.association.teamservice.team.eventstream;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.eventstream.team.TeamBroadcastMessage;
import nl.kristalsoftware.ddd.eventstream.base.producer.ByteArrayEventProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class TeamEventDataProducer {

    private final ByteArrayEventProducer eventProducer;

    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    @Value("${team-service.kafka.team.topicname}")
    private String topicname;

    public void produce(TeamBroadcastMessage teamBroadcastMessage) {
        eventProducer.produceEvent(
                kafkaTemplate,
                topicname,
                teamBroadcastMessage.getReference().toString(),
                teamBroadcastMessage.toByteArray());
    }

}
