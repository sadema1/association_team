package nl.kristalsoftware.association.teamservice;

import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;
import nl.kristalsoftware.association.teamservice.player.viewstore.PlayerDocumentRepository;
import nl.kristalsoftware.association.teamservice.player.viewstore.PlayerViewStoreDocumentAdapter;
import nl.kristalsoftware.association.teamservice.team.viewstore.TeamDocumentRepository;
import nl.kristalsoftware.association.teamservice.team.viewstore.TeamViewStoreDocumentAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanConfiguration {

    @Bean
    @Scope("prototype")
    public TeamViewStoreDocumentPort createTeamViewStoreDocumentAdapter(TeamDocumentRepository teamDocumentsRepository) {
        return new TeamViewStoreDocumentAdapter(teamDocumentsRepository);
    }

    @Bean
    @Scope("prototype")
    public PlayerViewStoreDocumentPort createPlayerViewStoreDocumentAdapter(PlayerDocumentRepository playerDocumentRepository) {
        return new PlayerViewStoreDocumentAdapter(playerDocumentRepository);
    }

}
