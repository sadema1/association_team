package nl.kristalsoftware.association.teamservice;

import nl.kristalsoftware.association.teamservice.domain.team.TeamCommandService;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.HashSet;
import java.util.Optional;

@SpringBootApplication
public class TeamServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeamServiceApplication.class, args);
    }

    @Profile("seed")
    @Bean
    CommandLineRunner seedTeams(TeamCommandService teamCommandService) {
        return args -> {
            Optional<TeamReference> teamReference = teamCommandService.registerTeam(
                    TeamName.of("JO17-1"),
                    TeamCategory.of("JO17"),
                    TeamDescription.of("Selectie team")
            );
            if (teamReference.isPresent()) {
                try {
                    teamCommandService.changeTeamAttributes(
                            teamReference.get(),
                            TeamName.of("JO15-1"),
                            TeamCategory.of("JO15"),
                            TeamDescription.of("Selectie team"),
                            new HashSet<>()
                    );
                } catch (AggregateNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }
}
