package nl.kristalsoftware.association.teamservice.team.viewstore.rest.command;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class TeamRegisterRequestBody {
    @NotEmpty
    private String teamName;
    private String teamCategory;
    private String teamDescription;
}
