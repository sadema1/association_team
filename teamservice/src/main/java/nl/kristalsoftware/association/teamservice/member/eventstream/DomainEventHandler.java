package nl.kristalsoftware.association.teamservice.member.eventstream;

public interface DomainEventHandler<T> {
    String appliesTo();

    void handleEvent(T eventData);
}
