package nl.kristalsoftware.association.teamservice.member.eventstream;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.ddd.eventstream.base.offsetmanagement.TopicPartitionHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GenericEventConsumer<T> {
    private final Map<String, DomainEventHandler<T>> eventMap;
    private final TopicPartitionHandler topicPartitionHandler;

    public GenericEventConsumer(List<DomainEventHandler<T>> eventList, TopicPartitionHandler topicPartitionHandler) {
        this.eventMap = eventList.stream().collect(Collectors.toMap(it -> it.appliesTo(), Function.identity()));
        this.topicPartitionHandler = topicPartitionHandler;
    }

    @Transactional
    public void handleMemberDomainEvent(String domainEventName, T memberEventData, String topic, int partition, long offset) {
        log.info("DomainEventName: {}", domainEventName);
        DomainEventHandler<T> domainEventHandler = eventMap.get(domainEventName);
        if (domainEventHandler != null) {
            domainEventHandler.handleEvent(memberEventData);
        }
        else {
            log.debug("No domain event handler found for event: {}", domainEventName);
        }
        log.debug("Save offset {} on partition {} of topic {}", offset, partition, topic);
        topicPartitionHandler.save(topic, partition, offset);
    }
}
