package nl.kristalsoftware.association.teamservice.player.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "PlayerRolesEvent")
public class PlayerRolesEntity extends PlayerBaseEventEntity<PlayerRolesAllocated> {

    @OneToMany
    @JoinColumn(name = "player_id")
    private Set<PlayerRoleAssignedEntity> playerRolesAssigned = new HashSet<>();

    @OneToMany
    @JoinColumn(name = "player_id")
    private Set<PlayerRoleUnassignedEntity> playerRolesUnassigned = new HashSet<>();

    private PlayerRolesEntity(UUID reference,
                              String domainEventName) {
        super(reference, domainEventName);
    }

    public static PlayerRolesEntity of(PlayerRolesAllocated playerRolesAllocated) {
        PlayerRolesEntity playerRolesEntity = new PlayerRolesEntity(
                playerRolesAllocated.getPlayerReference().getValue(),
                playerRolesAllocated.getClass().getSimpleName()
        );
        for (PlayerRole playerRole : playerRolesAllocated.getPlayerRolesAssigned()) {
            playerRolesEntity.addPlayerRoleAssigned(PlayerRoleAssignedEntity.of(playerRole));
        }
        for (PlayerRole playerRole : playerRolesAllocated.getPlayerRolesUnassigned()) {
            playerRolesEntity.addPlayerRoleUnassigned(PlayerRoleUnassignedEntity.of(playerRole));
        }
        return playerRolesEntity;
    }

    public PlayerRolesAllocated getDomainEvent() {
        Set<PlayerRole> playerRolesAssigned = getPlayerRolesAssigned().stream()
                .map(it -> it.toPlayerRole())
                .collect(Collectors.toSet());
        Set<PlayerRole> playerRolesUnassigned = getPlayerRolesUnassigned().stream()
                .map(it -> it.toPlayerRole())
                .collect(Collectors.toSet());
        return PlayerRolesAllocated.of(
                PlayerReference.of(getReference()),
                playerRolesAssigned,
                playerRolesUnassigned
        );
    }

    public void addPlayerRoleAssigned(PlayerRoleAssignedEntity playerRoleAssignedEntity) {
        playerRolesAssigned.add(playerRoleAssignedEntity);
        playerRoleAssignedEntity.setPlayerRolesEvent(this);
    }

    public void addPlayerRoleUnassigned(PlayerRoleUnassignedEntity playerRoleUnassignedEntity) {
        playerRolesUnassigned.add(playerRoleUnassignedEntity);
        playerRoleUnassignedEntity.setPlayerRolesEvent(this);
    }
}
