package nl.kristalsoftware.association.teamservice.team.eventstore;

import org.springframework.data.repository.CrudRepository;

public interface PlayerAssignedRepository extends CrudRepository<PlayerAssignedToTeamEntity,Long> {
}
