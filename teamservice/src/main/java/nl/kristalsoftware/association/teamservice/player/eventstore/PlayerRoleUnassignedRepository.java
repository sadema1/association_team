package nl.kristalsoftware.association.teamservice.player.eventstore;

import org.springframework.data.repository.CrudRepository;

public interface PlayerRoleUnassignedRepository extends CrudRepository<PlayerRoleUnassignedEntity,Long> {
}
