package nl.kristalsoftware.association.teamservice.team.viewstore;

import nl.kristalsoftware.association.teamservice.team.viewstore.rest.view.TeamResponseBody;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamDocumentRepository extends MongoRepository<TeamDocument, ObjectId> {
    Optional<TeamDocument> findByReference(UUID reference);

    Optional<TeamDocument> findByName(String teamName);

    @Aggregation(pipeline = {
            "{'$lookup': {'from': 'playerDocument', 'localField': 'playerReferences', 'foreignField': 'reference', 'as': 'players'}}"
    })
    List<TeamResponseBody> getAllTeamResponses();

    @Aggregation(pipeline = {
            "{'$match' : {'reference': UUID(?0)}}",
            "{'$lookup': {'from': 'playerDocument', 'localField': 'playerReferences', 'foreignField': 'reference', 'as': 'players'}}"
    })
    Optional<TeamResponseBody> getTeamByReferenceResponse(@Param("reference") UUID teamReference);
}
