package nl.kristalsoftware.association.teamservice.player.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "PlayerRoleUnassignedEvent")
public class PlayerRoleUnassignedEntity {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAYER_ROLE_UNASSIGNED_EVENT_ENTITY_SEQ")
    @SequenceGenerator(name="PLAYER_ROLE_UNASSIGNED_EVENT_ENTITY_SEQ", sequenceName = "PLAYER_ROLE_UNASSIGNED_EVENT_ENTITY_SEQ", allocationSize=1)
    private Long id;

    private String role;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private PlayerRolesEntity playerRolesEvent;

    private PlayerRoleUnassignedEntity(String role) {
        this.role = role;
    }

    public static PlayerRoleUnassignedEntity of(PlayerRole playerRole) {
        return new PlayerRoleUnassignedEntity(
                playerRole.getValue().name()
        );
    }

    public PlayerRole toPlayerRole() {
        return PlayerRole.of(role);
    }
}
