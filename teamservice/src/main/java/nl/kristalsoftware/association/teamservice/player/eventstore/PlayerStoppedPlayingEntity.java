package nl.kristalsoftware.association.teamservice.player.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;

import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "PlayerStoppedPlayingEvent")
public class PlayerStoppedPlayingEntity extends PlayerBaseEventEntity<PlayerStoppedPlaying> {
    private String kind;

    private PlayerStoppedPlayingEntity(UUID reference, String domainEventName, String kind) {
        super(reference, domainEventName);
        this.kind = kind;
    }

    public static PlayerStoppedPlayingEntity of(PlayerStoppedPlaying playerStoppedPlaying) {
        return new PlayerStoppedPlayingEntity(
                playerStoppedPlaying.getPlayerReference().getValue(),
                playerStoppedPlaying.getClass().getSimpleName(),
                playerStoppedPlaying.getPlayerKind().getValue().name()
        );
    }

    @Transient
    @Override
    public PlayerStoppedPlaying getDomainEvent() {
        return PlayerStoppedPlaying.of(
                PlayerReference.of(getReference()),
                PlayerKind.of(kind)
        );
    }
}
