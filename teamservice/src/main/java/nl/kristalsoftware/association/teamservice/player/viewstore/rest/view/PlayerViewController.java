package nl.kristalsoftware.association.teamservice.player.viewstore.rest.view;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.player.viewstore.PlayerDocumentRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/players", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Players view", description = "Endpoints for retrieving players")
public class PlayerViewController {

    private final PlayerDocumentRepository playerDocumentsRepository;

    @GetMapping
    @Operation(
            summary = "Get all players or a player by name",
            description = "Get all players or a player by name",
            tags = "Players view",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
            }
    )
    public List<PlayerResponseBody> getAllPlayers() {
        return playerDocumentsRepository.getAllPlayers();
    }

    @GetMapping(value = "/{playerReference}")
    @Operation(
            summary = "Get player by reference",
            description = "Get player by reference",
            tags = "Players view",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "Not Found", responseCode = "404"),
            }
    )
    public ResponseEntity<PlayerResponseBody> getPlayerByReference(@PathVariable String playerReference) {
        Optional<PlayerResponseBody> playerResponseBody = playerDocumentsRepository.getPlayerResponseByReference(UUID.fromString(playerReference));
        if (playerResponseBody.isPresent()) {
            return ResponseEntity.ok(playerResponseBody.get());
        }
        return ResponseEntity.notFound().build();
    }

}
