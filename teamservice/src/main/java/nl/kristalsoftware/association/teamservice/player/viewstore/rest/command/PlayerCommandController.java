package nl.kristalsoftware.association.teamservice.player.viewstore.rest.command;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerCommandService;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/players", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Players command", description = "Endpoints for update players")
public class PlayerCommandController {

    private final PlayerCommandService playerCommandService;

    @PutMapping(value = "/{playerReference}")
    @Operation(
            summary = "Edit player by reference",
            description = "Edit player by reference",
            tags = "Player commands",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "204"),
                    @ApiResponse(description = "Not Found", responseCode = "404"),
            }
    )
    public ResponseEntity<Void> changePlayerAttributes(
            @NotNull @PathVariable String playerReference, @RequestBody PlayerChangeAttributesRequestBody playerChangeAttributesRequestBody
    ) {
        try {
            playerCommandService.changePlayerAttributes(
                    PlayerReference.of(playerReference),
                    playerChangeAttributesRequestBody.getRoles().stream()
                            .map(PlayerRole::of)
                            .collect(Collectors.toSet())
            );
        } catch (AggregateNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
