package nl.kristalsoftware.association.teamservice.player.eventstream;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerBroadcastMessage;
import nl.kristalsoftware.ddd.eventstream.base.producer.ByteArrayEventProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class PlayerEventDataProducer {

    private final ByteArrayEventProducer eventProducer;

    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    @Value("${team-service.kafka.player.topicname}")
    private String topicname;

    public void produce(PlayerBroadcastMessage playerBroadcastMessage) {
        eventProducer.produceEvent(
                kafkaTemplate,
                topicname,
                playerBroadcastMessage.getReference().toString(),
                playerBroadcastMessage.toByteArray());
    }

}
