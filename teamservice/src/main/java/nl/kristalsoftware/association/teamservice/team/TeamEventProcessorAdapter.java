package nl.kristalsoftware.association.teamservice.team;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.team.TeamEventProcessorPort;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;
import nl.kristalsoftware.association.teamservice.team.eventstore.TeamEventStoreAdapter;
import nl.kristalsoftware.association.teamservice.team.eventstream.TeamEventStreamingAdapter;
import nl.kristalsoftware.ddd.domain.base.event.EventStorePort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TeamEventProcessorAdapter implements TeamEventProcessorPort {

    private final TeamEventStoreAdapter teamEventsRepositoryAdapter;

    private final TeamEventStreamingAdapter teamEventsStreamingAdapter;

    @Override
    public void saveEvent(TeamRegistered teamRegistered, TeamViewStoreDocumentPort teamViewStorePort) {
        teamEventsRepositoryAdapter.saveEvent(teamRegistered);
        teamEventsStreamingAdapter.streamEvent(teamRegistered);
        teamViewStorePort.saveEvent(teamRegistered);
    }

    @Override
    public void saveEvent(Team team, TeamAttributesChanged teamAttributesChanged, TeamViewStoreDocumentPort documentsRepositoryPort) {
        teamEventsRepositoryAdapter.saveEvent(teamAttributesChanged);
        teamEventsStreamingAdapter.streamEvent(team, teamAttributesChanged);
        documentsRepositoryPort.saveEvent(teamAttributesChanged);

    }

    @Override
    public void saveEvent(Team team, TeamPlayersAllocated teamPlayersAllocated, TeamViewStoreDocumentPort documentsRepositoryPort) {
        teamEventsRepositoryAdapter.saveEvent(teamPlayersAllocated);
        teamEventsStreamingAdapter.streamEvent(team, teamPlayersAllocated);
        documentsRepositoryPort.saveEvent(teamPlayersAllocated);
    }

    @Override
    public EventStorePort<Team, TeamReference> getEventStorePort() {
        return teamEventsRepositoryAdapter;
    }

}
