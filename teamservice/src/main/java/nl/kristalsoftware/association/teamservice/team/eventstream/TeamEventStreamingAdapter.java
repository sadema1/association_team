package nl.kristalsoftware.association.teamservice.team.eventstream;


import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.association.teamservice.domain.team.streams.TeamEventsStreamingPort;
import nl.kristalsoftware.association.teamservice.eventstream.team.TeamAggregateMsgPart;
import nl.kristalsoftware.association.teamservice.eventstream.team.TeamBroadcastMessage;
import nl.kristalsoftware.association.teamservice.eventstream.team.TeamEventMsgPart;
import nl.kristalsoftware.association.teamservice.eventstream.team.UUID_String;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TeamEventStreamingAdapter implements TeamEventsStreamingPort {

    private final TeamEventDataProducer teamEventDataProducer;

    @Override
    public void streamEvent(TeamRegistered teamRegistered) {
        TeamEventMsgPart teamEventMsgPart = TeamEventMsgPart.newBuilder()
                .setTeamName(teamRegistered.getTeamName().getValue())
                .setTeamCategory(teamRegistered.getTeamCategory().getValue())
                .setTeamDescription(teamRegistered.getTeamDescription().getValue())
                .build();
        TeamBroadcastMessage teamBroadcastMessage = TeamBroadcastMessage.newBuilder()
                .setReference(UUID_String.newBuilder()
                        .setValue(teamRegistered.getTeamReference().getStringValue())
                        .build())
                .setDomainEventName(teamRegistered.getClass().getSimpleName())
                .setEventData(teamEventMsgPart)
                .build();
        teamEventDataProducer.produce(teamBroadcastMessage);
    }

    @Override
    public void streamEvent(Team team, TeamAttributesChanged teamAttributesChanged) {
        if (teamAttributesChanged.getTeamName().isPresent() ||
                teamAttributesChanged.getTeamCategory().isPresent() ||
                teamAttributesChanged.getTeamDescription().isPresent()
        ) {
            TeamEventMsgPart teamEventMsgPart = TeamEventMsgPart.newBuilder()
                    .setTeamName(teamAttributesChanged.getTeamName().isPresent() ? teamAttributesChanged.getTeamName().get().getValue() : "")
                    .setTeamCategory(teamAttributesChanged.getTeamCategory().isPresent() ? teamAttributesChanged.getTeamCategory().get().getValue() : "")
                    .setTeamDescription(teamAttributesChanged.getTeamDescription().isPresent() ? teamAttributesChanged.getTeamDescription().get().getValue() : "")
                    .build();
            TeamAggregateMsgPart teamAggregateMsgPart = buildTeamAggregateMsgPart(team);
            TeamBroadcastMessage teamBroadcastMessage = TeamBroadcastMessage.newBuilder()
                    .setReference(UUID_String.newBuilder()
                            .setValue(team.getReference().getStringValue())
                            .build()
                    )
                    .setDomainEventName(teamAttributesChanged.getClass().getSimpleName())
                    .setEventData(teamEventMsgPart)
                    .setAggregateData(teamAggregateMsgPart)
                    .build();
            teamEventDataProducer.produce(teamBroadcastMessage);
        }
    }

    @Override
    public void streamEvent(Team team, TeamPlayersAllocated teamPlayersAllocated) {
        TeamEventMsgPart teamEventMsgPart = TeamEventMsgPart.newBuilder()
                .addAllAssignedPlayers(teamPlayersAllocated.getPlayersAssigned().stream()
                        .map(it -> UUID_String.newBuilder()
                                .setValue(it.getStringValue())
                                .build()
                        ).toList()
                )
                .addAllUnassignedPlayers(teamPlayersAllocated.getPlayersUnassigned().stream()
                        .map(it -> UUID_String.newBuilder()
                                .setValue(it.getStringValue())
                                .build()
                        ).toList()
                )
                .build();
        TeamAggregateMsgPart teamAggregateMsgPart = buildTeamAggregateMsgPart(team);
        TeamBroadcastMessage teamBroadcastMessage = TeamBroadcastMessage.newBuilder()
                .setReference(UUID_String.newBuilder()
                        .setValue(team.getReference().getStringValue())
                        .build()
                )
                .setDomainEventName(teamPlayersAllocated.getClass().getSimpleName())
                .setEventData(teamEventMsgPart)
                .setAggregateData(teamAggregateMsgPart)
                .build();
        teamEventDataProducer.produce(teamBroadcastMessage);
    }

    private TeamAggregateMsgPart buildTeamAggregateMsgPart(Team team) {
        TeamAggregateMsgPart teamAggregateMsgPart = TeamAggregateMsgPart.newBuilder()
                .setTeamName(team.getTeamName().getValue())
                .setTeamCategory(team.getTeamCategory().getValue())
                .setTeamDescription(team.getTeamDescription().getValue())
                .addAllPlayers(team.getTeamPlayersCollection().getValue().stream()
                        .map(it -> UUID_String.newBuilder()
                                .setValue(it.getStringValue())
                                .build()
                        ).toList()
                )
                .build();
        return teamAggregateMsgPart;
    }
}
