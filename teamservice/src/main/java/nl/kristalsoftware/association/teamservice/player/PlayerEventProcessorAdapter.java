package nl.kristalsoftware.association.teamservice.player;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerEventProcessorPort;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;
import nl.kristalsoftware.association.teamservice.player.eventstore.PlayerEventStoreAdapter;
import nl.kristalsoftware.association.teamservice.player.eventstream.PlayerEventStreamingAdapter;
import nl.kristalsoftware.ddd.domain.base.event.EventStorePort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class PlayerEventProcessorAdapter implements PlayerEventProcessorPort {

    private final PlayerEventStoreAdapter playerEventStoreAdapter;

    private final PlayerEventStreamingAdapter playerEventStreamingAdapter;

    @Override
    public void saveEvent(PlayerSignedUp playerSignedUp, PlayerViewStoreDocumentPort playerViewStoreDocumentPort) {
        playerEventStoreAdapter.saveEvent(playerSignedUp);
        playerEventStreamingAdapter.streamEvent(playerSignedUp);
        playerViewStoreDocumentPort.saveEvent(playerSignedUp);
    }

    @Override
    public void saveEvent(Player aggregate, PlayerStoppedPlaying playerStoppedPlaying, PlayerViewStoreDocumentPort playerViewStoreDocumentPort) {
        playerEventStoreAdapter.saveEvent(playerStoppedPlaying);
        playerEventStreamingAdapter.streamEvent(aggregate, playerStoppedPlaying);
        playerViewStoreDocumentPort.saveEvent(playerStoppedPlaying);
    }

    @Override
    public void saveEvent(Player aggregate, PlayerRolesAllocated playerRolesAllocated, PlayerViewStoreDocumentPort playerViewStoreDocumentPort) {
        playerEventStoreAdapter.saveEvent(playerRolesAllocated);
        playerEventStreamingAdapter.streamEvent(aggregate, playerRolesAllocated);
        playerViewStoreDocumentPort.saveEvent(playerRolesAllocated);
    }

    @Override
    public EventStorePort<Player, PlayerReference> getEventStorePort() {
        return playerEventStoreAdapter;
    }
}
