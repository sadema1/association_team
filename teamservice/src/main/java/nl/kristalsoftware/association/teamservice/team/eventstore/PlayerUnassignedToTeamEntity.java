package nl.kristalsoftware.association.teamservice.team.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "PlayerUnassignedToTeam")
public class PlayerUnassignedToTeamEntity {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAYER_UNASSIGNED_TO_TEAM_SEQ")
    @SequenceGenerator(name="PLAYER_UNASSIGNED_TO_TEAM_SEQ", sequenceName = "PLAYER_UNASSIGNED_TO_TEAM_SEQ", allocationSize=1)
    private Long id;

    private UUID playerReference;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private TeamPlayersEventEntity teamPlayersEventEntity;

    private PlayerUnassignedToTeamEntity(UUID playerReference) {
        this.playerReference = playerReference;
    }

    public static PlayerUnassignedToTeamEntity of(PlayerReference playerReference) {
        return new PlayerUnassignedToTeamEntity(playerReference.getValue());
    }

    public PlayerReference toPlayerReference() {
        return PlayerReference.of(playerReference);
    }
}
