package nl.kristalsoftware.association.teamservice.team.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "PlayerAssignedToTeam")
public class PlayerAssignedToTeamEntity {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAYER_ASSIGNED_TO_TEAM_SEQ")
    @SequenceGenerator(name="PLAYER_ASSIGNED_TO_TEAM_SEQ", sequenceName = "PLAYER_ASSIGNED_TO_TEAM_SEQ", allocationSize=1)
    private Long id;

    private UUID playerReference;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private TeamPlayersEventEntity teamPlayersEventEntity;

    private PlayerAssignedToTeamEntity(UUID playerReference) {
        this.playerReference = playerReference;
    }

    public static PlayerAssignedToTeamEntity of(PlayerReference playerReference) {
        return new PlayerAssignedToTeamEntity(playerReference.getValue());
    }

    public PlayerReference toPlayerReference() {
        return PlayerReference.of(playerReference);
    }
}
