package nl.kristalsoftware.association.teamservice.player.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerBirthDate;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerName;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "PlayerSignedUpEvent")
public class PlayerSignedUpEntity extends PlayerBaseEventEntity<PlayerSignedUp> {
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String kind;

    private PlayerSignedUpEntity(UUID reference, String domainEventName, String firstName, String lastName, LocalDate birthDate, String kind) {
        super(reference, domainEventName);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.kind = kind;
    }

    public static PlayerSignedUpEntity of(PlayerSignedUp playerSignedUp) {
        return new PlayerSignedUpEntity(
                playerSignedUp.getPlayerReference().getValue(),
                playerSignedUp.getClass().getSimpleName(),
                playerSignedUp.getPlayerName().getFirstName(),
                playerSignedUp.getPlayerName().getLastName(),
                playerSignedUp.getPlayerBirthDate().getValue(),
                playerSignedUp.getPlayerKind().getValue().name()
        );
    }

    @Transient
    @Override
    public PlayerSignedUp getDomainEvent() {
        return PlayerSignedUp.of(
                PlayerReference.of(getReference()),
                PlayerName.of(firstName, lastName),
                PlayerBirthDate.of(birthDate),
                PlayerKind.of(kind),
                PlayerRole.of(PlayerRole.Role.UNKNOWN.name())
        );
    }
}
