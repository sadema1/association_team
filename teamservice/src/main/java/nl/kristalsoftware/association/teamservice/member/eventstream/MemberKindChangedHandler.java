package nl.kristalsoftware.association.teamservice.member.eventstream;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberEventData;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerCommandService;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class MemberKindChangedHandler implements DomainEventHandler<MemberEventData> {

    private final PlayerCommandService playerCommandService;

    @Override
    public String appliesTo() {
        return "MemberKindChanged";
    }

    @Override
    public void handleEvent(MemberEventData eventData) {
        playerCommandService.handleMemberKindChanged(
                eventData.getReference().getValue(),
                eventData.getAggregateData().getFirstName(),
                eventData.getAggregateData().getLastName(),
                eventData.getAggregateData().getBirthDate(),
                eventData.getEventData().getKind()
        );
    }

}
