package nl.kristalsoftware.association.teamservice.team.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

import java.util.Optional;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "TeamAttributesChangedEvent")
public class TeamAttributesChangedEntity extends TeamBaseEventEntity<TeamAttributesChanged> {

    private String name;

    private String category;

    private String description;

    public TeamAttributesChangedEntity(UUID reference, String domainEventName, String name, String category, String description) {
        super(reference, domainEventName);
        this.name = name;
        this.category = category;
        this.description = description;
    }

    public static TeamAttributesChangedEntity of(TeamAttributesChanged teamAttributesChanged) {
        return new TeamAttributesChangedEntity(
                teamAttributesChanged.getTeamReference().getValue(),
                teamAttributesChanged.getClass().getSimpleName(),
                getOptionalStringAttribute(teamAttributesChanged.getTeamName()),
                getOptionalStringAttribute(teamAttributesChanged.getTeamCategory()),
                getOptionalStringAttribute(teamAttributesChanged.getTeamDescription())
        );
    }

    private static <T extends TinyStringType> String getOptionalStringAttribute(Optional<T> attribute) {
        return attribute.isPresent() ? attribute.get().getValue() : null;
    }

    @Transient
    @Override
    public TeamAttributesChanged getDomainEvent() {
        return TeamAttributesChanged.of(
                TeamReference.of(getReference()),
                name != null ? Optional.of(TeamName.of(name)) : Optional.empty(),
                category != null ? Optional.of(TeamCategory.of(category)) : Optional.empty(),
                description != null ? Optional.of(TeamDescription.of(description)) : Optional.empty()
        );
    }
}
