package nl.kristalsoftware.association.teamservice.player.viewstore.rest.view;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlayerTeamResponseBody {
    private UUID reference;
    private String name;
    private String category;
    private String description;
}
