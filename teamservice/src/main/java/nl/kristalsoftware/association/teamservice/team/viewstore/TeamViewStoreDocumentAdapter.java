package nl.kristalsoftware.association.teamservice.team.viewstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TeamViewStoreDocumentAdapter implements TeamViewStoreDocumentPort {

    private final TeamDocumentRepository teamDocumentsRepository;

    private TeamDocument teamDocument = new TeamDocument();

    @Override
    public void createDocument(Team aggregate) {
        teamDocument = teamDocumentsRepository.findByReference(aggregate.getReference().getValue())
                .orElseThrow(() -> new IllegalStateException(String.format("Document with reference %s not found in viewstore!", aggregate.getReference().getValue())));
    }

    @Override
    public void saveEvent(TeamRegistered teamRegistered) {
        teamDocument = TeamDocument.of(
                teamRegistered.getTeamReference().getValue(),
                teamRegistered.getTeamName().getValue(),
                teamRegistered.getTeamCategory().getValue(),
                teamRegistered.getTeamDescription().getValue()
        );
    }

    @Override
    public void saveEvent(TeamAttributesChanged teamAttributesChanged) {
        teamAttributesChanged.getTeamName().ifPresent(it -> teamDocument.setName(it.getValue()));
        teamAttributesChanged.getTeamCategory().ifPresent(it -> teamDocument.setCategory(it.getValue()));
        teamAttributesChanged.getTeamDescription().ifPresent(it -> teamDocument.setDescription(it.getValue()));
    }

    @Override
    public void saveEvent(TeamPlayersAllocated playerTeamsAllocated) {
        Set<UUID> newAssignedPlayers = playerTeamsAllocated.getPlayersAssigned().stream()
                .map(it -> it.getValue())
                .collect(Collectors.toSet());
        teamDocument.assignPlayers(newAssignedPlayers);
        Set<UUID> newUnassignedPlayers = playerTeamsAllocated.getPlayersUnassigned().stream()
                .map(it -> it.getValue())
                .collect(Collectors.toSet());
        teamDocument.unassignPlayers(newUnassignedPlayers);
    }


    @Override
    public void saveDocument() {
        teamDocumentsRepository.save(teamDocument);
    }

}
