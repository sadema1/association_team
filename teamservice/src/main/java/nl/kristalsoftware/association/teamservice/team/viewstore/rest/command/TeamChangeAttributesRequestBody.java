package nl.kristalsoftware.association.teamservice.team.viewstore.rest.command;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class TeamChangeAttributesRequestBody {
    @NotEmpty
    private String teamName;
    private String teamCategory;
    private String teamDescription;
    private Set<UUID> players;
}
