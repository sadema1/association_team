package nl.kristalsoftware.association.teamservice.team.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "TeamRegisteredEvent")
public class TeamRegisteredEntity extends TeamBaseEventEntity<TeamRegistered> {

    private String name;

    private String category;

    private String description;

    public TeamRegisteredEntity(UUID reference, String domainEventName, String name, String category, String description) {
        super(reference, domainEventName);
        this.name = name;
        this.category = category;
        this.description = description;
    }

    public static TeamRegisteredEntity of(TeamRegistered teamRegistered) {
        return new TeamRegisteredEntity(
                teamRegistered.getTeamReference().getValue(),
                teamRegistered.getClass().getSimpleName(),
                teamRegistered.getTeamName().getValue(),
                teamRegistered.getTeamCategory().getValue(),
                teamRegistered.getTeamDescription().getValue()
        );
    }

    @Transient
    @Override
    public TeamRegistered getDomainEvent() {
        return TeamRegistered.of(
                TeamReference.of(getReference()),
                TeamName.of(name),
                TeamCategory.of(category),
                TeamDescription.of(description)
        );
    }

}
