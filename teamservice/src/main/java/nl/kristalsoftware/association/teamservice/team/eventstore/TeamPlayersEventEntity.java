package nl.kristalsoftware.association.teamservice.team.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "TeamPlayersEvent")
public class TeamPlayersEventEntity extends TeamBaseEventEntity<TeamPlayersAllocated> {

    @OneToMany
    @JoinColumn(name = "team_id")
    private Set<PlayerAssignedToTeamEntity> playersAssigned = new HashSet<>();

    @OneToMany
    @JoinColumn(name = "team_id")
    private Set<PlayerUnassignedToTeamEntity> playersUnassigned = new HashSet<>();

    private TeamPlayersEventEntity(UUID reference, String domainEventName) {
        super(reference, domainEventName);
    }

    public static TeamPlayersEventEntity of(TeamPlayersAllocated teamPlayersAllocated) {
        TeamPlayersEventEntity teamPlayersEntity = new TeamPlayersEventEntity(
                teamPlayersAllocated.getTeamReference().getValue(),
                teamPlayersAllocated.getClass().getSimpleName()
        );
        for (PlayerReference playerReference : teamPlayersAllocated.getPlayersAssigned()) {
            teamPlayersEntity.addPlayerAssigned(PlayerAssignedToTeamEntity.of(playerReference));
        }
        for (PlayerReference teamReference : teamPlayersAllocated.getPlayersUnassigned()) {
            teamPlayersEntity.addPlayerUnassigned(PlayerUnassignedToTeamEntity.of(teamReference));
        }
        return teamPlayersEntity;
    }

    public TeamPlayersAllocated getDomainEvent() {
        Set<PlayerReference> teamsAssigned = getPlayersAssigned().stream()
                .map(it -> it.toPlayerReference())
                .collect(Collectors.toSet());
        Set<PlayerReference> teamsUnassigned = getPlayersUnassigned().stream()
                .map(it -> it.toPlayerReference())
                .collect(Collectors.toSet());
        return TeamPlayersAllocated.of(
                TeamReference.of(getReference()),
                teamsAssigned,
                teamsUnassigned
        );
    }

    public void addPlayerAssigned(PlayerAssignedToTeamEntity playerAssignedToTeamEntity) {
        playersAssigned.add(playerAssignedToTeamEntity);
        playerAssignedToTeamEntity.setTeamPlayersEventEntity(this);
    }

    public void addPlayerUnassigned(PlayerUnassignedToTeamEntity playerUnassignedToTeamEntity) {
        playersUnassigned.add(playerUnassignedToTeamEntity);
        playerUnassignedToTeamEntity.setTeamPlayersEventEntity(this);
    }
}
