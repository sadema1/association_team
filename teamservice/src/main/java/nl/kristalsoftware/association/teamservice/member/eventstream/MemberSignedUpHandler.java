package nl.kristalsoftware.association.teamservice.member.eventstream;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberEventData;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerCommandService;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class MemberSignedUpHandler implements DomainEventHandler<MemberEventData> {
    private final PlayerCommandService playerCommandService;

    @Override
    public String appliesTo() {
        return "MemberSignedUp";
    }

    @Override
    public void handleEvent(MemberEventData eventData) {
        log.info("Handle the event: {}", "MemberSignedUp");
        playerCommandService.handleMemberSignedUpEvent(
                eventData.getReference().getValue(),
                eventData.getEventData().getFirstName(),
                eventData.getEventData().getLastName(),
                eventData.getEventData().getBirthDate(),
                eventData.getEventData().getKind()
        );
    }

}
