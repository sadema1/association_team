package nl.kristalsoftware.association.teamservice.member.eventstream;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.Map;

@RequiredArgsConstructor
@Configuration
public class KafkaConsumerConfig {
    private final KafkaProperties kafkaProperties;

    @Bean
    public ConsumerFactory<String,byte[]> consumerFactory() {
        Map<String,Object> consumerConfig = kafkaProperties.buildConsumerProperties();
        DefaultKafkaConsumerFactory<String,byte[]> factory = new DefaultKafkaConsumerFactory<>(consumerConfig);
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String,byte[]> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String,byte[]> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setConcurrency(4);
        return factory;
    }

}
