package nl.kristalsoftware.association.teamservice;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {
        "nl.kristalsoftware.ddd.eventstore.base",
        "nl.kristalsoftware.ddd.eventstream.base",
        "nl.kristalsoftware.association.teamservice.team.eventstore",
        "nl.kristalsoftware.association.teamservice.player.eventstore"
})
@EntityScan(basePackages = {
        "nl.kristalsoftware.ddd.eventstore.base",
        "nl.kristalsoftware.ddd.eventstream.base",
        "nl.kristalsoftware.association.teamservice.team.eventstore",
        "nl.kristalsoftware.association.teamservice.player.eventstore"
})
@Configuration
public class JPAConfiguration {
}
