package nl.kristalsoftware.association.teamservice.team.eventstore;


import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TeamEventStoreRepository extends CrudRepository<TeamBaseEventEntity, Long> {

    Iterable<TeamBaseEventEntity<? extends DomainEventLoading<Team>>> findAllByReference(UUID value);

    Optional<TeamBaseEventEntity<?>> findFirstByReference(UUID value);

}
