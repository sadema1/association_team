package nl.kristalsoftware.association.teamservice.player.eventstore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerEventStorePort;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class PlayerEventStoreAdapter implements PlayerEventStorePort {

    private final PlayerEventStoreRepository playerEventStoreRepository;

    private final PlayerRoleAssignedRepository playerRoleAssignedRepository;

    private final PlayerRoleUnassignedRepository playerRoleUnassignedRepository;

    @Override
    public List<DomainEventLoading<Player>> findAllDomainEventsByReference(Player aggregate) {
        List<DomainEventLoading<Player>> list = new ArrayList<>();
        for (PlayerBaseEventEntity<? extends DomainEventLoading<Player>> it : playerEventStoreRepository.findAllByReference(aggregate.getReference().getValue())) {
            DomainEventLoading<Player> domainEvent = it.getDomainEvent();
            list.add(domainEvent);
        }
        return list;
    }

    @Override
    public boolean eventsExists(PlayerReference playerReference) {
        return playerEventStoreRepository.findFirstByReference(playerReference.getValue()).isPresent();
    }

    @Override
    public void saveEvent(PlayerSignedUp playerSignedUp) {
        playerEventStoreRepository.save(PlayerSignedUpEntity.of(playerSignedUp));
    }

    @Override
    public void saveEvent(PlayerStoppedPlaying playerStoppedPlaying) {
        playerEventStoreRepository.save(PlayerStoppedPlayingEntity.of(playerStoppedPlaying));
    }

    @Override
    public void saveEvent(PlayerRolesAllocated playerRolesAllocated) {
        PlayerRolesEntity playerRolesEntity = PlayerRolesEntity.of(playerRolesAllocated);
        playerEventStoreRepository.save(playerRolesEntity);
        for (PlayerRoleAssignedEntity playerRoleAssignedEntity : playerRolesEntity.getPlayerRolesAssigned()) {
            playerRoleAssignedRepository.save(playerRoleAssignedEntity);
        }
        for (PlayerRoleUnassignedEntity playerRoleUnassignedEntity : playerRolesEntity.getPlayerRolesUnassigned()) {
            playerRoleUnassignedRepository.save(playerRoleUnassignedEntity);
        }
    }

}
