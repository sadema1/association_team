package nl.kristalsoftware.association.teamservice.team.viewstore.rest.command;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.TeamCommandService;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamCategory;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamDescription;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamName;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/teams", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Team commands", description = "Endpoints for updating teams")
public class TeamCommandController {

    private final TeamCommandService teamCommandService;

    @PostMapping
    @Operation(
            summary = "register team",
            description = "Register team",
            tags = "Team commands",
            responses = {
                    @ApiResponse(description = "Created", responseCode = "201"),
                    @ApiResponse(description = "Unprocessable Entity", responseCode = "422"),
            }
    )
    public ResponseEntity<Void> createTeam(@RequestBody TeamRegisterRequestBody teamRegisterRequestBody) {
        Optional<TeamReference> teamReference = teamCommandService.registerTeam(
                TeamName.of(teamRegisterRequestBody.getTeamName()),
                TeamCategory.of(teamRegisterRequestBody.getTeamCategory()),
                TeamDescription.of(teamRegisterRequestBody.getTeamDescription())
        );
        if (teamReference.isPresent()) {
            return ResponseEntity.created(URI.create("/teams/" + teamReference.get().getValue().toString())).build();
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @PutMapping(value = "/{teamReference}", consumes = "application/json")
    @Operation(
            summary = "Change team attributes",
            description = "Change team attributes by reference",
            tags = "Team commands",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "Not Found", responseCode = "404"),
            }
    )
    public ResponseEntity<Void> changeTeamAttributes(@PathVariable String teamReference, @RequestBody TeamChangeAttributesRequestBody teamChangeAttributesRequestBody) {
        try {
            teamCommandService.changeTeamAttributes(
                    TeamReference.of(UUID.fromString(teamReference)),
                    TeamName.of(teamChangeAttributesRequestBody.getTeamName()),
                    TeamCategory.of(teamChangeAttributesRequestBody.getTeamCategory()),
                    TeamDescription.of(teamChangeAttributesRequestBody.getTeamDescription()),
                    teamChangeAttributesRequestBody.getPlayers().stream()
                            .map(it -> PlayerReference.of(it))
                            .collect(Collectors.toSet())
            );
        } catch (AggregateNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
