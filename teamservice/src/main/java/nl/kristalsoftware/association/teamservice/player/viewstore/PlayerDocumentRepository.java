package nl.kristalsoftware.association.teamservice.player.viewstore;

import nl.kristalsoftware.association.teamservice.player.viewstore.rest.view.PlayerResponseBody;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PlayerDocumentRepository extends MongoRepository<PlayerDocument, ObjectId> {

    Optional<PlayerDocument> findByReference(UUID reference);

    @Aggregation(pipeline = {
            "{'$match': {'reference': UUID(?0)}}",
            "{'$lookup': {'from': 'teamDocument', 'localField': 'reference', 'foreignField': 'playerReferences', 'as': 'teams'}}"
    })
    Optional<PlayerResponseBody> getPlayerResponseByReference(@Param("reference") UUID reference);

    @Aggregation(pipeline = {
            "{'$lookup': {'from': 'teamDocument', 'localField': 'teamReferences', 'foreignField': 'reference', 'as': 'teams'}}"
    })
    List<PlayerResponseBody> getAllPlayers();
}
