package nl.kristalsoftware.association.teamservice.team.viewstore.rest.view;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.team.viewstore.TeamDocumentRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/teams", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Teams view", description = "Endpoints for retrieving teams")
public class TeamViewController {

    private final TeamDocumentRepository teamDocumentsRepository;

    @GetMapping
    @Operation(
            summary = "Get all teams or a team by name",
            description = "Get all teams or a team by name",
            tags = "Teams view",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
            }
    )
    public List<TeamResponseBody> getAllTeams() {
        return teamDocumentsRepository.getAllTeamResponses();
    }

    @GetMapping(value = "/{teamReference}")
    @Operation(
            summary = "Get team by reference",
            description = "Get team by reference",
            tags = "Teams view",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "Not Found", responseCode = "404"),
            }
    )
    public ResponseEntity<TeamResponseBody> getTeamByReference(@PathVariable String teamReference) {
        Optional<TeamResponseBody> optionalTeamResponseBody = teamDocumentsRepository.getTeamByReferenceResponse(UUID.fromString(teamReference));
        if (optionalTeamResponseBody.isPresent()) {
            return ResponseEntity.ok(optionalTeamResponseBody.get());
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

}
