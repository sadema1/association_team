package nl.kristalsoftware.association.teamservice.player.eventstore;

import org.springframework.data.repository.CrudRepository;

public interface PlayerRoleAssignedRepository extends CrudRepository<PlayerRoleAssignedEntity,Long> {
}
