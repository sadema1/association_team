package nl.kristalsoftware.association.teamservice.team.viewstore.rest.view;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class TeamPlayerResponseBody {
    private UUID reference;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private PlayerKind.Kind kind;
    private Set<String> roles;
}
