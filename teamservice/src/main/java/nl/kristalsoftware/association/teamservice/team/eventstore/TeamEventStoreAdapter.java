package nl.kristalsoftware.association.teamservice.team.eventstore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamAttributesChanged;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamEventStorePort;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamPlayersAllocated;
import nl.kristalsoftware.association.teamservice.domain.team.events.TeamRegistered;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class TeamEventStoreAdapter implements TeamEventStorePort {

    private final TeamEventStoreRepository eventStoreRepository;

    private final PlayerAssignedRepository playerAssignedRepository;

    private final PlayerUnassignedRepository playerUnassignedRepository;

    @Override
    public List<DomainEventLoading<Team>> findAllDomainEventsByReference(Team aggregate) {
        List<DomainEventLoading<Team>> list = new ArrayList<>();
        for (TeamBaseEventEntity<? extends DomainEventLoading<Team>> it : eventStoreRepository.findAllByReference(aggregate.getReference().getValue())) {
            DomainEventLoading<Team> domainEvent = it.getDomainEvent();
            list.add(domainEvent);
        }
        return list;
    }

    @Override
    public boolean eventsExists(TeamReference teamReference) {
        return eventStoreRepository.findFirstByReference(teamReference.getValue()).isPresent();
    }

    @Override
    public void saveEvent(TeamRegistered teamRegistered) {
        eventStoreRepository.save(TeamRegisteredEntity.of(teamRegistered));
    }

    @Override
    public void saveEvent(TeamAttributesChanged teamAttributesChanged) {
        eventStoreRepository.save(TeamAttributesChangedEntity.of(teamAttributesChanged));
    }

    @Override
    public void saveEvent(TeamPlayersAllocated teamPlayersAllocated) {
        TeamPlayersEventEntity teamPlayersEventEntity = TeamPlayersEventEntity.of(teamPlayersAllocated);
        eventStoreRepository.save(teamPlayersEventEntity);
        for (PlayerAssignedToTeamEntity playerAssignedToTeamEntity : teamPlayersEventEntity.getPlayersAssigned()) {
            playerAssignedRepository.save(playerAssignedToTeamEntity);
        }
        for (PlayerUnassignedToTeamEntity playerUnassignedToTeamEntity : teamPlayersEventEntity.getPlayersUnassigned()) {
            playerUnassignedRepository.save(playerUnassignedToTeamEntity);
        }
    }

}
