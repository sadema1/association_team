package nl.kristalsoftware.association.teamservice.member.eventstream;

import com.google.protobuf.InvalidProtocolBufferException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberEventData;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class MemberEventConsumer {

    private final GenericEventConsumer<MemberEventData> genericEventConsumer;

    @KafkaListener(topics = "${member-service.kafka.member.topicname}")
    public void consumeMemberEventData(@Payload ConsumerRecord<String, byte[]> record) throws InvalidProtocolBufferException {
        log.info("Thread: {}, Partition: {}, Key: {}, Topic: {}, Offset: {}",
                Thread.currentThread().getId(), record.partition(), record.key(), record.topic(), record.offset());
        MemberEventData memberEventData = MemberEventData.parseFrom(record.value());
        genericEventConsumer.handleMemberDomainEvent(
                memberEventData.getDomainEventName(),
                memberEventData,
                record.topic(),
                record.partition(),
                record.offset()
        );
        log.info("Thread: {} Processing MemberEventData finished...!", Thread.currentThread().getId());
    }

}
