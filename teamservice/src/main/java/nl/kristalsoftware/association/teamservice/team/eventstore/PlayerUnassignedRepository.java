package nl.kristalsoftware.association.teamservice.team.eventstore;

import org.springframework.data.repository.CrudRepository;

public interface PlayerUnassignedRepository extends CrudRepository<PlayerUnassignedToTeamEntity,Long> {
}
