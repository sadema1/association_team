package nl.kristalsoftware.association.teamservice.team.eventstore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.SequenceGenerator;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity
@Inheritance(
        strategy = InheritanceType.JOINED
)
public abstract class TeamBaseEventEntity<T extends DomainEventLoading> {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEAM_BASE_EVENT_ENTITY_SEQ")
    @SequenceGenerator(name="TEAM_BASE_EVENT_ENTITY_SEQ", sequenceName = "TEAM_BASE_EVENT_ENTITY_SEQ", allocationSize=1)
    private Long id;

    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID reference;

    private String domainEventName;

    @Setter(AccessLevel.NONE)
    @CreationTimestamp
    private LocalDateTime creationDateTime;

    public TeamBaseEventEntity(UUID reference, String domainEventName) {
        this.reference = reference;
        this.domainEventName = domainEventName;
    }

    public abstract T getDomainEvent();
}
