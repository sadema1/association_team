package nl.kristalsoftware.association.teamservice.team.viewstore;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Document
public class TeamDocument {

    @Setter(AccessLevel.NONE)
    @Id
    private ObjectId _id;
    private UUID reference;
    private String name;
    private String category;
    private String description;
    private Set<UUID> playerReferences = new LinkedHashSet<>();
    @Version
    private Long version;

    private TeamDocument(UUID reference, String name, String category, String description) {
        this.reference = reference;
        this.name = name;
        this.category = category;
        this.description = description;
    }

    public static TeamDocument of(UUID reference, String name, String category, String description) {
        return new TeamDocument(reference, name, category, description);
    }

    public void assignPlayers(Set<UUID> newAssignedPlayers) {
        playerReferences.addAll(newAssignedPlayers);
    }

    public void unassignPlayers(Set<UUID> newUnassignedPlayers) {
        playerReferences.removeAll(newUnassignedPlayers);
    }

}
