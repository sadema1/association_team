package nl.kristalsoftware.association.teamservice.player.eventstream;


import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerRolesAllocated;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerSignedUp;
import nl.kristalsoftware.association.teamservice.domain.player.events.PlayerStoppedPlaying;
import nl.kristalsoftware.association.teamservice.domain.player.streams.PlayerEventsStreamingPort;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerAggregateMsgPart;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerBroadcastMessage;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerEventMsgPart;
import nl.kristalsoftware.association.teamservice.eventstream.player.UUID_String;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class PlayerEventStreamingAdapter implements PlayerEventsStreamingPort {

    private final PlayerEventDataProducer playerEventDataProducer;

    @Override
    public void streamEvent(PlayerSignedUp playerSignedUp) {
        PlayerEventMsgPart playerData = PlayerEventMsgPart.newBuilder()
                .setFirstName(playerSignedUp.getPlayerName().getFirstName())
                .setLastName(playerSignedUp.getPlayerName().getLastName())
                .setBirthDate(playerSignedUp.getPlayerBirthDate().getDateInMillis())
                .setKind(playerSignedUp.getPlayerKind().getValue().name())
                .build();
        PlayerBroadcastMessage playerBroadcastMessage = PlayerBroadcastMessage.newBuilder()
                .setReference(UUID_String.newBuilder()
                        .setValue(playerSignedUp.getPlayerReference().getStringValue())
                        .build())
                .setDomainEventName(PlayerSignedUp.class.getSimpleName())
                .setEventData(playerData)
                .build();
        playerEventDataProducer.produce(playerBroadcastMessage);
    }

    @Override
    public void streamEvent(Player player, PlayerStoppedPlaying playerStoppedPlaying) {
        PlayerEventMsgPart playerData = PlayerEventMsgPart.newBuilder()
                .setKind(playerStoppedPlaying.getPlayerKind().getValue().name())
                .build();
        PlayerAggregateMsgPart playerAggregatePart = buildPlayerAggregateData(player);
        PlayerBroadcastMessage playerEventData = PlayerBroadcastMessage.newBuilder()
                .setReference(UUID_String.newBuilder()
                        .setValue(playerStoppedPlaying.getPlayerReference().getStringValue())
                        .build())
                .setDomainEventName(PlayerStoppedPlaying.class.getSimpleName())
                .setEventData(playerData)
                .setAggregateData(playerAggregatePart)
                .build();
        playerEventDataProducer.produce(playerEventData);
    }

    @Override
    public void streamEvent(Player player, PlayerRolesAllocated playerRolesAllocated) {
        PlayerEventMsgPart playerData = addAllAssignedAndUnassignedRoles(playerRolesAllocated);
        PlayerAggregateMsgPart playerAggregateData = buildPlayerAggregateData(player);
        PlayerBroadcastMessage playerEventData = PlayerBroadcastMessage.newBuilder()
                .setReference(UUID_String.newBuilder()
                        .setValue(playerRolesAllocated.getPlayerReference().getStringValue())
                        .build())
                .setDomainEventName(PlayerRolesAllocated.class.getSimpleName())
                .setEventData(playerData)
                .setAggregateData(playerAggregateData)
                .build();
        playerEventDataProducer.produce(playerEventData);
    }

    private PlayerEventMsgPart addAllAssignedAndUnassignedRoles(PlayerRolesAllocated playerRolesAllocated) {
        return PlayerEventMsgPart.newBuilder()
                .addAllAssignedRoles(playerRolesAllocated.getPlayerRolesAssigned().stream()
                        .map(it -> it.getValue().name())
                        .collect(Collectors.toSet())
                )
                .addAllUnassignedRoles(playerRolesAllocated.getPlayerRolesUnassigned().stream()
                        .map(it -> it.getValue().name())
                        .collect(Collectors.toSet())
                )
                .build();
    }

    private PlayerAggregateMsgPart buildPlayerAggregateData(Player player) {
        return PlayerAggregateMsgPart.newBuilder()
                .setFirstName(player.getPlayerDomainEntity().getPlayerName().getFirstName())
                .setLastName(player.getPlayerDomainEntity().getPlayerName().getLastName())
                .setBirthDate(player.getPlayerDomainEntity().getPlayerBirthDate().getDateInMillis())
                .setKind(player.getPlayerDomainEntity().getPlayerKind().getValue().name())
                .addAllRoles(player.getPlayerDomainEntity().getPlayerRoleCollection().getValue().stream()
                        .map(it -> it.getValue().name())
                        .collect(Collectors.toSet()))
                .build();
    }
}
