package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.player.viewstore.rest.command.PlayerChangeAttributesRequestBody;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlayerCommandSteps extends TestContainers {

    private final ScenarioReferences scenarioReferences;

    private final WebClient webClient;

    @When("assign roles {string}")
    public void assign_roles(String roles) {
        final var rolesAsSet = ScenarioHelper.createSetOfString(roles);
        String uriOfPlayer = "/players/" + scenarioReferences.getPlayerTailReference().getStringValue();
        log.info("PUT {}", uriOfPlayer);
        PlayerChangeAttributesRequestBody requestBody = PlayerChangeAttributesRequestBody.of(
                rolesAsSet,
                Set.of()
        );
        webClient
                .put()
                .uri(uriOfPlayer)
                .bodyValue(requestBody)
                .exchange()
                .block();
    }

    @When("the Player is assigned to the Team")
    public void the_player_is_assigned_to_the_team() {
        String uriOfPlayer = "/players/" + scenarioReferences.getPlayerTailReference().getStringValue();
        log.info("PUT {}", uriOfPlayer);
//        webClient
//                .put()
//                .uri(uriOfPlayer)
//                .bodyValue()
//                .exchange()
//                .expectStatus().isNoContent();
    }

}
