package nl.kristalsoftware.association.teamservice.cucumber;

import lombok.Getter;
import lombok.Setter;
import nl.kristalsoftware.association.teamservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Component
public class ScenarioReferences {

    @Getter
    @Setter
    private MemberReference memberReference;

    @Getter
    private Set<PlayerReference> playerReferences = new LinkedHashSet<>();

    @Getter
    private Set<TeamReference> teamReferences = new LinkedHashSet<>();

    public void initializeReferences() {
        playerReferences = new LinkedHashSet<>();
        teamReferences = new LinkedHashSet<>();
    }

    public void setPlayerReference(PlayerReference playerReference) {
        playerReferences.add(playerReference);
    }

    public void setTeamReference(TeamReference teamReference) {
        teamReferences.add(teamReference);
    }

    public PlayerReference getPlayerTailReference() {
        List<PlayerReference> referenceList = playerReferences.stream().toList();
        if (referenceList.size() > 0) {
            return referenceList.get(referenceList.size()-1);
        }
        return PlayerReference.of("");
    }

    public TeamReference getTeamReference() {
        return teamReferences.stream().findFirst().orElseThrow();
    }
}
