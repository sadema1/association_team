package nl.kristalsoftware.association.teamservice.cucumber;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

@Slf4j
@RequiredArgsConstructor
@Component
public class BroadcastMessageProvider<T> {

    private final KafkaConsumer<String, byte[]> kafkaConsumer;

    public List<T> getBroadcastMessages(Function<byte[],T> parseMessageFunction, Function<T,String> eventNameSupplier, Set<String> eventNames) {
        List<T> broadcastMessages = new ArrayList<>();
        await().pollInterval(1, SECONDS)
                .atMost(5, SECONDS)
                .until(() -> {
                    ConsumerRecords<String, byte[]> records = kafkaConsumer.poll(Duration.ofMillis(500));
                    for (ConsumerRecord<String, byte[]> record : records) {
                        T broadcastMessage = parseMessageFunction.apply(record.value());
                        log.info(broadcastMessage.toString());
                        String domainEventName = eventNameSupplier.apply(broadcastMessage);
                        if (eventNames.contains(domainEventName)) {
                            log.info("Add {} to broadcastMessages!", domainEventName);
                            broadcastMessages.add(broadcastMessage);
                        }
                    }
                    return broadcastMessages.size() == eventNames.size();
                });
        return broadcastMessages;
    }

}
