package nl.kristalsoftware.association.teamservice.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberEventData;
import nl.kristalsoftware.ddd.eventstream.base.producer.ByteArrayEventProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class MemberEventDataProducer {

    private final ByteArrayEventProducer eventProducer;

    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    @Value("${member-service.kafka.member.topicname}")
    private String topicname;

    public void produce(MemberEventData memberEventData) {
        eventProducer.produceEvent(
                kafkaTemplate,
                topicname,
                memberEventData.getReference().toString(),
                memberEventData.toByteArray());
    }

}
