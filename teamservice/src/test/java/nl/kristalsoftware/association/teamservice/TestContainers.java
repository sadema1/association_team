package nl.kristalsoftware.association.teamservice;

import eu.rekawek.toxiproxy.Proxy;
import eu.rekawek.toxiproxy.ToxiproxyClient;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.ToxiproxyContainer;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
public class TestContainers {

    static Network network = Network.newNetwork();


    static final MariaDBContainer mariadb = new MariaDBContainer(
            DockerImageName.parse("mariadb")
    ).withDatabaseName("team");


    static final MongoDBContainer mongodb = new MongoDBContainer(DockerImageName.parse("mongo"))
            .withNetwork(network)
            .withExposedPorts(27017)
            .withNetworkAliases("mongodb")
//            .withEnv("MONGODB_ROOT_PASSWORD", "welkom123")
//            .withEnv("MONGO_INITDB_ROOT_USERNAME", "team")
//            .withEnv("MONGO_INITDB_ROOT_PASSWORD", "team")
            .withEnv("MONGO_INITDB_DATABASE", "team")
            ;
    static final KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.2.1"))
            .withNetwork(network);

    static final ToxiproxyContainer toxiproxy = new ToxiproxyContainer("ghcr.io/shopify/toxiproxy:2.5.0")
            .withNetwork(network);

    @DynamicPropertySource
    static void configureMariaDbTestProperties(DynamicPropertyRegistry registry) throws IOException {
        mariadb.withNetwork(network);
        Startables.deepStart(mariadb, mongodb, kafka, toxiproxy).join();

        final ToxiproxyClient toxiproxyClient = new ToxiproxyClient(toxiproxy.getHost(), toxiproxy.getControlPort());
        final Proxy proxy = toxiproxyClient.createProxy("mongodb_proxy", "0.0.0.0:8666", "mongodb:27017");

        final String ipAddressViaToxiproxy = toxiproxy.getHost();
        final int toxiProxyControlPort = toxiproxy.getControlPort();
        final int portViaToxiproxy = toxiproxy.getMappedPort(8666);

        log.info("proxyHost: {}, controlPort: {}, mappedPort: {}", ipAddressViaToxiproxy, toxiProxyControlPort, portViaToxiproxy);
        log.info("mongodb mapped port: {}", mongodb.getMappedPort(27017));
//        Integer mongodbMappedPort = mongodb.getMappedPort(27017);
//        String upstream = "mongodb:" + mongodbMappedPort;

//        final int toxiProxyMappedMongoProxyPort = toxiproxy.getMappedPort(8666);
//        String listen = "0.0.0.0:" + toxiProxyMappedMongoProxyPort;
//        final Proxy proxy = toxiproxyClient.createProxy("mongodb_proxy", listen, upstream);
//
//        log.info("Created proxy with listen: {} and upstream: {}", listen, upstream);

//        final int portViaToxiproxy = toxiproxy.getMappedPort(8666);
//        log.info("toxiProxyMappedMongoProxyPort: {}", toxiProxyMappedMongoProxyPort);
//        log.info("toxiProxyControlPort: {}", toxiProxyControlPort);
//        log.info("ipAddressViaToxiproxy: {}", ipAddressViaToxiproxy);
//        log.info("portViaToxiproxy: {}", portViaToxiproxy);

        registry.add("spring.datasource.url", mariadb::getJdbcUrl);
        registry.add("spring.datasource.username", mariadb::getUsername);
        registry.add("spring.datasource.password", mariadb::getPassword);

        String mongoConnectionString = "mongodb://" + ipAddressViaToxiproxy + ":" + portViaToxiproxy + "/team";
        registry.add("spring.data.mongodb.uri", () -> mongoConnectionString);
        registry.add("spring.data.mongodb.host", () -> ipAddressViaToxiproxy);
        registry.add("spring.data.mongodb.port", () -> portViaToxiproxy);
//        registry.add("spring.data.mongodb.password", mongodb::getPassword);
//        registry.add("spring.data.mongodb.database", mongodb::getPassword);

        registry.add("spring.kafka.bootstrap-servers", kafka::getBootstrapServers);
        log.info("spring.kafka.bootstrap-servers: {}", kafka.getBootstrapServers());

        log.info("======== {} {} {} ========", mariadb.getJdbcUrl(), mariadb.getUsername(), mariadb.getPassword());
        log.info("======== {} ========", mongoConnectionString);
    }
}
