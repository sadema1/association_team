package nl.kristalsoftware.association.teamservice.cucumber;

import com.google.protobuf.InvalidProtocolBufferException;
import io.cucumber.java.en.Then;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.eventstream.team.TeamBroadcastMessage;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TeamAssertBroadcastSteps extends TestContainers {

    private final ScenarioReferences scenarioReferences;
    private final BroadcastMessageProvider<TeamBroadcastMessage> broadcastMessageProvider;

    @Then("The events {string} have been broadcasted with properties teamName {string} and category {string} and description {string} and number of players {int} and number of assigned players {int}")
    public void the_events_have_been_broadcasted_with_properties_team_name_and_category_and_description_and_number_of_players(String eventsAsString, String teamName, String category, String description, int numberOfPlayers, int numberOfAssignedPlayers) {
        Set<String> events = ScenarioHelper.createSetOfString(eventsAsString);
        List<TeamBroadcastMessage> messages = broadcastMessageProvider.getBroadcastMessages(
                TeamAssertBroadcastSteps::parseMessageFromBytes,
                TeamAssertBroadcastSteps::getEventName,
                events
        );
        for (int msgIndex = 0; msgIndex < messages.size(); msgIndex++) {
            TeamBroadcastMessage teamBroadcastMessage = messages.get(msgIndex);
            assertThat(teamBroadcastMessage.getAggregateData().getTeamName()).isEqualTo(teamName);
            assertThat(teamBroadcastMessage.getAggregateData().getTeamCategory()).isEqualTo(category);
            assertThat(teamBroadcastMessage.getAggregateData().getTeamDescription()).isEqualTo(description);
            List<UUID> playerReferences = teamBroadcastMessage.getAggregateData().getPlayersList().stream()
                    .map(it -> UUID.fromString(it.getValue()))
                    .toList();
            assertThat(playerReferences.size()).isEqualTo(numberOfPlayers);
            List<UUID> assignedPlayerReferences = teamBroadcastMessage.getEventData().getAssignedPlayersList().stream()
                    .map(it -> UUID.fromString(it.getValue()))
                    .toList();
            assertThat(assignedPlayerReferences.size()).isEqualTo(numberOfAssignedPlayers);
        }
    }

    private static String getEventName(TeamBroadcastMessage message) {
        return message.getDomainEventName();
    }

    private static TeamBroadcastMessage parseMessageFromBytes(byte[] streamValue) {
        try {
            return TeamBroadcastMessage.parseFrom(streamValue);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }
}
