package nl.kristalsoftware.association.teamservice.cucumber;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class CucumberBootstrap extends TestContainers {
}
