package nl.kristalsoftware.association.teamservice.cucumber;

import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScenarioHelper {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static Set<String> createSetOfString(String roles) {
        Set<String> rolesAsSet = roles.isEmpty() ? Set.of() :
                Stream.of(roles.trim().split("\\s*,\\s*"))
                        .collect(Collectors.toSet());
        return rolesAsSet;
    }

    @SneakyThrows
    public static long parseDate(String date) {
        if (date == null || date.isEmpty()) {
            return 0;
        }
        return sdf.parse(date).getTime();
    }
}
