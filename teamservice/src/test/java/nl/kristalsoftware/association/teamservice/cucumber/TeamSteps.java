package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.domain.team.attributes.TeamReference;
import nl.kristalsoftware.association.teamservice.team.viewstore.rest.command.TeamChangeAttributesRequestBody;
import nl.kristalsoftware.association.teamservice.team.viewstore.rest.command.TeamRegisterRequestBody;
import nl.kristalsoftware.association.teamservice.team.viewstore.rest.view.TeamPlayerResponseBody;
import nl.kristalsoftware.association.teamservice.team.viewstore.rest.view.TeamResponseBody;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@RequiredArgsConstructor
@CucumberContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TeamSteps extends TestContainers {

    private final WebTestClient webTestClient;

    private final ScenarioReferences scenarioReferences;

    private String uriOfTeam;

    @SneakyThrows
    @Given("A new created team with name {string} in category {string} with description {string}")
    public void a_new_created_team_with_name_in_category_with_description(String teamName, String teamCategory, String teamDescription) {
        TeamRegisterRequestBody teamRegisterRequestBody = TeamRegisterRequestBody.of(
                teamName,
                teamCategory,
                teamDescription
        );
        uriOfTeam = webTestClient
                .post()
                .uri("/teams")
                .bodyValue(teamRegisterRequestBody)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().value("Location", it -> it.contains("/teams/"))
                .returnResult(String.class).getResponseHeaders().get("Location").get(0)
        ;
        log.info(uriOfTeam);
        scenarioReferences.setTeamReference(TeamReference.of(uriOfTeam.replaceFirst(Pattern.quote("/teams/"), "")));
    }

    @Then("I should find one team with name {string} in category {string} with description {string} and number of players {int}")
    public void iShouldFindOneTeamWithNameInCategoryWithDescriptionAndNumberOfPlayers(String teamName, String teamCategory, String teamDescription, int numOfPlayers) {
        webTestClient
                .get()
                .uri(uriOfTeam)
                .exchange()
                .expectStatus().isOk()
                .expectBody(TeamResponseBody.class).value(it -> {
                    assertThat(it).isNotNull();
                    assertThat(uriOfTeam).matches("/teams/" + it.getReference());
                    assertThat(it.getName()).isEqualTo(teamName);
                    assertThat(it.getCategory()).isEqualTo(teamCategory);
                    assertThat(it.getDescription()).isEqualTo(teamDescription);
                    assertThat(it.getPlayers().length).isEqualTo(numOfPlayers);
                    List<UUID> playerReferences = scenarioReferences.getPlayerReferences().stream().map(ref -> ref.getValue()).toList();
                    for (TeamPlayerResponseBody player : it.getPlayers()) {
                        assertThat(player.getReference()).isIn(playerReferences);
                        log.info("Player with reference {} found", player.getReference());
                    }
                });
    }

    @When("the team is changed with name {string} in category {string} with description {string}")
    public void theTeamIsChangedWithNameInCategoryWithDescription(String teamName, String teamCategory, String teamDescription) {
        TeamChangeAttributesRequestBody teamChangeAttributesRequestBody = TeamChangeAttributesRequestBody.of(
                teamName,
                teamCategory,
                teamDescription,
                new HashSet<>()
        );
        webTestClient
                .put()
                .uri(uriOfTeam)
                .bodyValue(teamChangeAttributesRequestBody)
                .exchange()
                .expectStatus().isNoContent();
    }
}
