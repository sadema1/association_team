package nl.kristalsoftware.association.teamservice.cucumber;

import com.google.protobuf.InvalidProtocolBufferException;
import io.cucumber.java.en.Then;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerAggregateMsgPart;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerBroadcastMessage;
import nl.kristalsoftware.association.teamservice.eventstream.player.PlayerEventMsgPart;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlayerAssertBroadcastSteps extends TestContainers {

    private final BroadcastMessageProvider<PlayerBroadcastMessage> broadcastMessageProvider;

//    private final KafkaConsumer<String, byte[]> kafkaConsumer;
//
//    @NotNull
//    private List<PlayerBroadcastMessage> getPlayerBroadcastMessages(Predicate<String> eventNameCondition, int expectedNumberOfEvents) {
//        List<PlayerBroadcastMessage> broadcastMessages = new ArrayList<>();
//        await().pollInterval(1, SECONDS)
//                .atMost(5, SECONDS)
//                .until(() -> {
//                    ConsumerRecords<String, byte[]> records = kafkaConsumer.poll(Duration.ofMillis(500));
//                    for (ConsumerRecord<String, byte[]> record : records) {
//                        PlayerBroadcastMessage playerBroadcastMessage = PlayerBroadcastMessage.parseFrom(record.value());
//                        log.info(playerBroadcastMessage.toString());
//                        if (eventNameCondition.test(playerBroadcastMessage.getDomainEventName())) {
//                            log.info("Add {} to broadcastMessages!", playerBroadcastMessage.getDomainEventName());
//                            broadcastMessages.add(playerBroadcastMessage);
//                        }
//                    }
//                    return broadcastMessages.size() == expectedNumberOfEvents;
//                });
//        return broadcastMessages;
//    }

    @SneakyThrows
    @Then("The events {string} have been broadcasted with properties firstName {string} and lastName {string} and birthDate {string} and kind {string} and roles {string} and eventproperties firstName {string} and lastName {string} and birthDate {string} and kind {string} and assignedroles {string} and unassignedroles {string}")
    public void the_events_have_been_broadcasted_with_properties_first_name_and_last_name_and_birth_date_and_kind_and_roles_and_eventproperties_first_name_and_last_name_and_birth_date_and_kind_and_assignedroles_and_unassignedroles(String eventsAsString, String firstName, String lastName, String birthDate, String kind, String roles, String eventFirstName, String eventLastName, String eventBirthDate, String eventKind, String eventAssignedRoles, String eventUnassignedRoles) {
        log.info("Events: {}", eventsAsString);
        final var events = ScenarioHelper.createSetOfString(eventsAsString);
        final var rolesAsSet = ScenarioHelper.createSetOfString(roles);
        final var assignedRolesAsSet = ScenarioHelper.createSetOfString(eventAssignedRoles);
        final var unassignedRolesAsSet = ScenarioHelper.createSetOfString(eventUnassignedRoles);
        final var broadcastMessages = broadcastMessageProvider.getBroadcastMessages(PlayerAssertBroadcastSteps::parseMessageFromBytes, PlayerBroadcastMessage::getDomainEventName, events);
//        final var broadcastMessages = getPlayerBroadcastMessages(events::equals, eventsAsSet.size());
        for (int msgIndex = 0; msgIndex < broadcastMessages.size(); msgIndex++) {
            PlayerBroadcastMessage playerBroadcastMessage = broadcastMessages.get(msgIndex);
            PlayerAggregateMsgPart playerAggregateMsgPart = playerBroadcastMessage.getAggregateData();
            assertThat(playerBroadcastMessage.getAggregateData().getFirstName()).isEqualTo(firstName);
            assertThat(playerBroadcastMessage.getAggregateData().getLastName()).isEqualTo(lastName);
            assertThat(playerBroadcastMessage.getAggregateData().getBirthDate()).isEqualTo(ScenarioHelper.parseDate(birthDate));
            assertThat(playerBroadcastMessage.getAggregateData().getKind()).isEqualTo(kind);
            for (int rowIndex = 0; rowIndex < playerAggregateMsgPart.getRolesCount(); rowIndex++) {
                String role = playerAggregateMsgPart.getRoles(rowIndex);
                assertThat(role).isIn(rolesAsSet);
            }
            PlayerEventMsgPart playerEventMsgPart = playerBroadcastMessage.getEventData();
            for (int rowIndex = 0; rowIndex < playerEventMsgPart.getAssignedRolesCount(); rowIndex++) {
                String role = playerEventMsgPart.getAssignedRoles(rowIndex);
                assertThat(role).isIn(assignedRolesAsSet);
            }
            for (int rowIndex = 0; rowIndex < playerEventMsgPart.getUnassignedRolesCount(); rowIndex++) {
                String role = playerEventMsgPart.getUnassignedRoles(rowIndex);
                assertThat(role).isIn(unassignedRolesAsSet);
            }
            assertThat(playerEventMsgPart.getFirstName()).isEqualTo(eventFirstName);
            assertThat(playerEventMsgPart.getLastName()).isEqualTo(eventLastName);
            assertThat(playerEventMsgPart.getBirthDate()).isEqualTo(ScenarioHelper.parseDate(eventBirthDate));
            assertThat(playerEventMsgPart.getKind()).isEqualTo(eventKind);
        }
    }

    private static String getEventName(PlayerBroadcastMessage message) {
        return message.getDomainEventName();
    }

    private static PlayerBroadcastMessage parseMessageFromBytes(byte[] streamValue) {
        try {
            return PlayerBroadcastMessage.parseFrom(streamValue);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }
}
