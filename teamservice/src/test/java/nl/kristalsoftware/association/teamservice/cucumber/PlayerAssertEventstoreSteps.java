package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.Then;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerEventProcessorPort;
import nl.kristalsoftware.association.teamservice.domain.player.PlayerRepository;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.Player;
import nl.kristalsoftware.association.teamservice.domain.player.aggregate.PlayerDomainEntity;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerRole;
import nl.kristalsoftware.association.teamservice.domain.player.views.PlayerViewStoreDocumentPort;
import nl.kristalsoftware.association.teamservice.player.eventstore.PlayerBaseEventEntity;
import nl.kristalsoftware.association.teamservice.player.eventstore.PlayerEventStoreRepository;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlayerAssertEventstoreSteps extends TestContainers {

    private final ObjectProvider<PlayerViewStoreDocumentPort> objectProvider;
    private final ScenarioReferences scenarioReferences;
    private final PlayerEventStoreRepository playerEventStoreRepository;
    private final PlayerEventProcessorPort playerEventProcessorPort;

    private <T extends PlayerBaseEventEntity<? extends DomainEventLoading<Player>>> List<PlayerBaseEventEntity<? extends DomainEventLoading<Player>>> assertNumberOfEvents(Supplier<Iterable<T>> supplier, int expectedNumberOfEntities) {
        List<PlayerBaseEventEntity<? extends DomainEventLoading<Player>>> resultSet = new ArrayList<>();
        await().pollInterval(1, SECONDS)
                .atMost(Duration.ofSeconds(5))
                .pollInSameThread()
                .until(() -> {
                    Iterable<T> entities = supplier.get();
                    Set<T> entitySet = StreamSupport.stream(entities.spliterator(), false).collect(Collectors.toSet());
                    log.info("Found number of entities: {}", entitySet.size());
                    if (entitySet.size() == expectedNumberOfEntities) {
                        for (T entity : entitySet) {
                            resultSet.add(entity);
                        }
                        return true;
                    }
                    return false;
                });
        return resultSet;
    }

    @Transactional
    @Then("I should find one player aggregate with {int} events {string} and firstName {string} and lastName {string} and birthDate {string} and kind {string} and roles {string}")
    public void i_should_find_one_player_aggregate_with_events_and_first_name_and_last_name_and_birth_date_and_kind_and_roles(int expectedNumberOfEvents, String events, String firstName, String lastName, String birthDate, String kind, String roles) {
        final var eventsAsSet = ScenarioHelper.createSetOfString(events);
        final var rolesAsSet = ScenarioHelper.createSetOfString(roles);
        List<PlayerBaseEventEntity<? extends DomainEventLoading<Player>>> baseEvents = assertNumberOfEvents(
                () -> playerEventStoreRepository.findAllByReference(scenarioReferences.getPlayerTailReference().getValue()),
                expectedNumberOfEvents
        );
        for (int eventIndex = 0; eventIndex < expectedNumberOfEvents; eventIndex++) {
            PlayerBaseEventEntity<?> baseEventEntity = baseEvents.get(eventIndex);
            log.info("DomainEventName: {}", baseEventEntity.getDomainEventName());
            assertThat(baseEventEntity.getDomainEventName()).isIn(eventsAsSet);
        }
        PlayerRepository playerRepository = PlayerRepository.of(
                scenarioReferences.getPlayerTailReference(),
                playerEventProcessorPort,
                objectProvider.getObject());
        Player player = playerRepository.getAggregate();
        PlayerDomainEntity playerDomainEntity = player.getPlayerDomainEntity();
        assertThat(player.getReference().getStringValue()).isEqualTo(scenarioReferences.getPlayerTailReference().getStringValue());
        assertThat(playerDomainEntity.getPlayerName().getFirstName()).isEqualTo(firstName);
        assertThat(playerDomainEntity.getPlayerName().getLastName()).isEqualTo(lastName);
        assertThat(playerDomainEntity.getPlayerBirthDate().getValue()).isEqualTo(birthDate);
        assertThat(playerDomainEntity.getPlayerKind().getValue().name()).isEqualTo(kind);
        assertThat(playerDomainEntity.getPlayerRoleCollection().getValue().size()).isEqualTo(rolesAsSet.size());
        for (PlayerRole playerRole : playerDomainEntity.getPlayerRoleCollection().getValue()) {
            assertThat(playerRole.getValue().name()).isIn(rolesAsSet);
        }
//        assertThat(playerDomainEntity.getPlayerTeamCollection().getValue().size()).isEqualTo(scenarioReferences.getTeamReferences().size());
    }

    @SneakyThrows
    @Then("I should not find the player")
    public void i_should_not_find_the_player() {
        Thread.sleep(3000);
        assertThat(playerEventStoreRepository.findFirstByReference(
                UUID.fromString(scenarioReferences.getPlayerTailReference().getStringValue())).isEmpty()).isTrue();
    }

    @Then("the player is member of team with name {string} in category {string} with description {string}")
    public void the_player_is_member_of_team_with_name_in_category_with_description(String string, String string2, String string3) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

}
