package nl.kristalsoftware.association.teamservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.reactive.function.client.WebClient;

@Lazy
@Configuration
public class WebClientConfig {

    @Bean
    WebClient createWebClient(@Value("${local.server.port}") int port) {
        return WebClient.builder()
                .baseUrl("http://localhost:" + port)
                .build();
    }

}
