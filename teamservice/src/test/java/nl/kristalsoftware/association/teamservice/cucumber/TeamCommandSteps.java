package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.team.viewstore.rest.command.TeamChangeAttributesRequestBody;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TeamCommandSteps {

    private final ScenarioReferences scenarioReferences;

    private final WebClient webClient;

    @When("specify players for name {string} and category {string} and description {string}")
    public void specify_players_for_name_and_category_and_description(String teamName, String category, String description) {
        String uriOfPlayer = "/teams/" + scenarioReferences.getTeamReference().getStringValue();
        log.info("PUT {}", uriOfPlayer);
        TeamChangeAttributesRequestBody requestBody = TeamChangeAttributesRequestBody.of(
                teamName,
                category,
                description,
                scenarioReferences.getPlayerReferences().stream().map(it -> it.getValue()).collect(Collectors.toSet())
        );
        webClient
                .put()
                .uri(uriOfPlayer)
                .bodyValue(requestBody)
                .exchange()
                .block();
    }

}
