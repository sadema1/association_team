package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.Then;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerKind;
import nl.kristalsoftware.association.teamservice.player.viewstore.rest.view.PlayerResponseBody;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlayerAssertViewstoreSteps extends TestContainers {

    private final ScenarioReferences scenarioReferences;

    private final WebTestClient webTestClient;

    @Then("I should find one player with firstName {string} and lastName {string} and birthDate {string} and kind {string} and roles {string}")
    public void i_should_find_one_player_with_first_name_and_last_name_and_birth_date_and_kind_and_roles(String firstName, String lastName, String birthDate, String kind, String roles) {
        final var rolesAsSet = ScenarioHelper.createSetOfString(roles);
        String uriOfPlayer = "/players/" + scenarioReferences.getPlayerTailReference().getStringValue();
        log.info("GET {}", uriOfPlayer);
        List<PlayerResponseBody> playerResponseBodyList = new ArrayList<>();
        await().pollInterval(1, SECONDS)
                .atMost(5, SECONDS)
                .until(() -> {
                    webTestClient
                            .get()
                            .uri(uriOfPlayer)
                            .exchange()
                            .expectStatus().isOk()
                            .expectBody(PlayerResponseBody.class).value(it -> {
                                assertThat(uriOfPlayer).matches("/players/" + it.getReference());
                                assertThat(it.getFirstName()).isEqualTo(firstName);
                                assertThat(it.getLastName()).isEqualTo(lastName);
                                assertThat(it.getBirthDate()).isEqualTo(birthDate);
                                assertThat(it.getKind()).isEqualTo(PlayerKind.Kind.valueOf(kind));
                                assertThat(it.getRoles()).hasSameElementsAs(rolesAsSet);
//                                assertThat(it.getPlayerTeams().stream
//                                        .map(it -> it.getPlayerTeamReference())
//                                        .toSet()
//                                ).hasSameElementsAs(scenarioReferences.getTeamReferences());
                                playerResponseBodyList.add(it);
                            });
                    return playerResponseBodyList.size() == 1;
                });

    }

}
