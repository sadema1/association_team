package nl.kristalsoftware.association.teamservice.cucumber;

import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;

@Suite(failIfNoTests = false)
@IncludeEngines("cucumber")
@SelectClasspathResource("nl.kristalsoftware.association.teamservice.cucumber")
@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "nl.kristalsoftware.association.teamservice.cucumber")
public class CucumberIntegrationTest {
}
