package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.Then;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.domain.team.TeamEventProcessorPort;
import nl.kristalsoftware.association.teamservice.domain.team.TeamRepository;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.Team;
import nl.kristalsoftware.association.teamservice.domain.team.aggregate.TeamDomainEntity;
import nl.kristalsoftware.association.teamservice.domain.team.views.TeamViewStoreDocumentPort;
import nl.kristalsoftware.association.teamservice.team.eventstore.TeamBaseEventEntity;
import nl.kristalsoftware.association.teamservice.team.eventstore.TeamEventStoreRepository;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TeamAssertEventstoreSteps {

    private final ObjectProvider<TeamViewStoreDocumentPort> objectProvider;
    private final TeamEventStoreRepository teamEventStoreRepository;
    private final ScenarioReferences scenarioReferences;
    private final TeamEventProcessorPort teamEventProcessorPort;

    @Transactional
    @Then("I should find one team aggregate with {int} events {string} and teamName {string} and category {string} and description {string}")
    public void i_should_find_one_team_aggregate_with_events_and_team_name_and_category_and_description(int expectedNumberOfEvents, String events, String teamName, String category, String description) {
        final var eventsAsSet = ScenarioHelper.createSetOfString(events);
        List<TeamBaseEventEntity<? extends DomainEventLoading<Team>>> baseEvents = assertNumberOfEvents(
                () -> teamEventStoreRepository.findAllByReference(scenarioReferences.getTeamReference().getValue()),
                expectedNumberOfEvents
        );
        for (int eventIndex = 0; eventIndex < expectedNumberOfEvents; eventIndex++) {
            TeamBaseEventEntity<?> baseEventEntity = baseEvents.get(eventIndex);
            log.info("DomainEventName: {}", baseEventEntity.getDomainEventName());
            assertThat(baseEventEntity.getDomainEventName()).isIn(eventsAsSet);
        }
        TeamRepository teamRepository = TeamRepository.of(
                scenarioReferences.getTeamReference(),
                teamEventProcessorPort,
                objectProvider.getObject()
        );
        Team team = teamRepository.getAggregate();
        TeamDomainEntity teamDomainEntity = team.getTeamDomainEntity();
        assertThat(team.getReference().getStringValue()).isEqualTo(scenarioReferences.getTeamReference().getStringValue());
        assertThat(teamDomainEntity.getTeamName().getValue()).isEqualTo(teamName);
        assertThat(teamDomainEntity.getTeamCategory().getValue()).isEqualTo(category);
        assertThat(teamDomainEntity.getTeamDescription().getValue()).isEqualTo(description);
        assertThat(teamDomainEntity.getTeamPlayers().getValue().size()).isEqualTo(scenarioReferences.getPlayerReferences().size());
        for (PlayerReference playerReference : teamDomainEntity.getTeamPlayers().getValue()) {
            assertThat(playerReference).isIn(scenarioReferences.getPlayerReferences());
        }
    }

    private <T extends TeamBaseEventEntity<? extends DomainEventLoading<Team>>> List<TeamBaseEventEntity<? extends DomainEventLoading<Team>>> assertNumberOfEvents(Supplier<Iterable<T>> supplier, int expectedNumberOfEntities) {
        List<TeamBaseEventEntity<? extends DomainEventLoading<Team>>> resultSet = new ArrayList<>();
        await().pollInterval(1, SECONDS)
                .atMost(Duration.ofSeconds(5))
                .pollInSameThread()
                .until(() -> {
                    Iterable<T> entities = supplier.get();
                    Set<T> entitySet = StreamSupport.stream(entities.spliterator(), false).collect(Collectors.toSet());
                    log.info("Found number of entities: {}", entitySet.size());
                    if (entitySet.size() == expectedNumberOfEntities) {
                        for (T entity : entitySet) {
                            resultSet.add(entity);
                        }
                        return true;
                    }
                    return false;
                });
        return resultSet;
    }


}
