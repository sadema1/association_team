package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberData;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberEventData;
import nl.kristalsoftware.association.memberservice.eventstream.member.UUID_String;
import nl.kristalsoftware.association.teamservice.TestContainers;
import nl.kristalsoftware.association.teamservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.teamservice.domain.player.attributes.PlayerReference;
import nl.kristalsoftware.association.teamservice.kafka.MemberEventDataProducer;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MemberSteps extends TestContainers {

    private final MemberEventDataProducer memberEventDataProducer;

    private final ScenarioReferences scenarioReferences;

    @SneakyThrows
    @Given("a MemberSignedUp event with firstName {string} and lastName {string} and birthDate {string} and kind {string}")
    public void a_member_signed_up_event_with_first_name_and_last_name_and_birth_date_and_kind(String firstName, String lastName, String birthDate, String kind) {
        scenarioReferences.setMemberReference(MemberReference.of(UUID.randomUUID()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        MemberEventData memberEventData = MemberEventData.newBuilder()
                .setDomainEventName("MemberSignedUp")
                .setReference(UUID_String.newBuilder().setValue(scenarioReferences.getMemberReference().getStringValue()))
                .setEventData(MemberData.newBuilder()
                        .setFirstName(firstName)
                        .setLastName(lastName)
                        .setBirthDate(sdf.parse(birthDate).getTime())
                        .setKind(kind)
                        .build()
                )
                .build();
        scenarioReferences.setPlayerReference(PlayerReference.of(scenarioReferences.getMemberReference().getStringValue()));
        memberEventDataProducer.produce(memberEventData);
    }

    @SneakyThrows
    @When("received a MemberKindChanged event with firstName {string} and lastName {string} and birthDate {string} and kind {string} and newKind {string}")
    public void received_a_member_kind_changed_event_with_first_name_and_last_name_and_birth_date_and_kind_and_new_kind(String firstName, String lastName, String birthDate, String kind, String newKind) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        MemberEventData memberEventData = MemberEventData.newBuilder()
                .setDomainEventName("MemberKindChanged")
                .setReference(UUID_String.newBuilder().setValue(scenarioReferences.getMemberReference().getStringValue()))
                .setEventData(MemberData.newBuilder()
                        .setKind(newKind)
                        .build()
                )
                .setAggregateData(MemberData.newBuilder()
                        .setFirstName(firstName)
                        .setLastName(lastName)
                        .setBirthDate(sdf.parse(birthDate).getTime())
                        .setKind(kind)
                        .build())
                .build();
        memberEventDataProducer.produce(memberEventData);
    }
}
