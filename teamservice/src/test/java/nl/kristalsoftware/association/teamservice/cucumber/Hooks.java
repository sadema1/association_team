package nl.kristalsoftware.association.teamservice.cucumber;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import lombok.RequiredArgsConstructor;
import org.junit.Assume;

@RequiredArgsConstructor
public class Hooks {

    private final ScenarioReferences scenarioReferences;

    private static boolean scenarioFailed = false;
    private static String message;
    public static Scenario currentScenario;

    @Before
    public void beforeScenario(Scenario scenario) {
        scenarioReferences.initializeReferences();
        Hooks.currentScenario = scenario;
        Assume.assumeFalse(message, scenarioFailed);
    }

    @After
    public void afterScenario(Scenario scenario){
        if (scenario.isFailed()) {
            message = String.format("Scenario '%s' failed", scenario.getName());
            scenarioFailed  = true;
        }
    }

}
