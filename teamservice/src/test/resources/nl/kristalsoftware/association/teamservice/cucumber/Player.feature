# Created by sjoerdadema at 11/06/2023
Feature: PlayerEvents
  PlayerEvents

  Scenario Outline: Member signed up
    Given a MemberSignedUp event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind>
    Then I should find one player aggregate with 1 events <events> and firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles>
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles>
    And The events <events> have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and assignedroles "" and unassignedroles ""
    Examples:
      | firstName | lastName   | birthDate    | kind     | roles | events           |
      | "Niek"    | "Schokman" | "2003-01-01" | "PLAYER" |   ""  | "PlayerSignedUp" |

  Scenario Outline: Member kind changed from player to supporting member
    Given a MemberSignedUp event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind>
    Then I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles>
    And The events "PlayerSignedUp" have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and assignedroles "" and unassignedroles ""
    When received a MemberKindChanged event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and newKind <eventKind>
    Then I should find one player aggregate with 2 events <events> and firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <eventKind> and roles <roles>
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <eventKind> and roles <roles>
    And The events "PlayerStoppedPlaying" have been broadcasted with properties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles> and eventproperties firstName "" and lastName "" and birthDate "" and kind <eventKind> and assignedroles "" and unassignedroles ""
    Examples:
      | firstName | lastName     | birthDate    | kind     | roles | eventKind           | events                                |
      | "Nout"    | "de Keijzer" | "2003-03-15" | "PLAYER" | ""    | "SUPPORTING_MEMBER" | "PlayerSignedUp,PlayerStoppedPlaying" |

  Scenario Outline: Member kind changed from supporting member to player
    Given a MemberSignedUp event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind>
    Then I should not find the player
    When received a MemberKindChanged event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and newKind <eventKind>
    Then I should find one player aggregate with 1 events <events> and firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <eventKind> and roles <roles>
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <eventKind> and roles <roles>
    And The events "PlayerSignedUp" have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <eventKind> and assignedroles "" and unassignedroles ""
    Examples:
      | firstName | lastName     | birthDate    | kind                | roles | eventKind  | events           |
      | "Nout"    | "de Keijzer" | "2003-03-15" | "SUPPORTING_MEMBER" | ""    | "PLAYER"   | "PlayerSignedUp" |

  Scenario Outline: Assign roles to player
    Given a MemberSignedUp event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind>
    Then I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles>
    And The events "PlayerSignedUp" have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and assignedroles "" and unassignedroles ""
    When assign roles <newRoles>
    Then I should find one player aggregate with 2 events <events> and firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <newRoles>
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <newRoles>
    And The events "PlayerRolesAllocated" have been broadcasted with properties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles "" and eventproperties firstName "" and lastName "" and birthDate "" and kind "" and assignedroles <newRoles> and unassignedroles ""
    Examples:
      | firstName | lastName   | birthDate    | kind     | roles     | newRoles           | events                                |
      | "Olaf"    | "Lamotte"  | "2005-08-25" | "PLAYER" |    ""     | "STRIKER,MIDFIELD" | "PlayerSignedUp,PlayerRolesAllocated" |

  Scenario Outline: Unassign roles to player
    Given a MemberSignedUp event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind>
    Then I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <initialRoles>
    And The events "PlayerSignedUp" have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and assignedroles "" and unassignedroles ""
    When assign roles <roles>
    Then I should find one player aggregate with 2 events "PlayerSignedUp,PlayerRolesAllocated" and firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles>
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles>
    And The events "PlayerRolesAllocated" have been broadcasted with properties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles "" and eventproperties firstName "" and lastName "" and birthDate "" and kind "" and assignedroles <roles> and unassignedroles ""
    When assign roles <newRoles>
    Then I should find one player aggregate with 3 events "PlayerSignedUp,PlayerRolesAllocated,PlayerRolesAllocated" and firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <newRoles>
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <newRoles>
    And The events "PlayerRolesAllocated" have been broadcasted with properties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles <roles> and eventproperties firstName "" and lastName "" and birthDate "" and kind "" and assignedroles "STRIKER" and unassignedroles "DEFENDER,KEEPER"
    Examples:
      | firstName | lastName   | birthDate    | kind     | initialRoles | roles                      | newRoles           |
      | "Olaf"    | "Lamotte"  | "2005-08-25" | "PLAYER" |       ""     | "DEFENDER,MIDFIELD,KEEPER" | "STRIKER,MIDFIELD" |

