# Created by sjoerdadema at 21/03/2023
Feature: TeamEvents
  Team events

  Scenario: Create team
    Given A new created team with name "JO17-1" in category "JO17" with description "Selection Team"
    Then I should find one team with name "JO17-1" in category "JO17" with description "Selection Team" and number of players 0

  Scenario: Change team attributes
    Given A new created team with name "JO18-3" in category "JO-18" with description "Ordinary Team"
    When the team is changed with name "JO18-4" in category "JO18" with description "Friends team"
    Then I should find one team with name "JO18-4" in category "JO18" with description "Friends team" and number of players 0

  Scenario Outline: Player assigned to team
    Given A new created team with name <teamName> in category <category> with description <description>
    And a MemberSignedUp event with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind>
    Then I should find one team with name <teamName> in category <category> with description <description> and number of players 0
    And I should find one player with firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and roles ""
    And The events "PlayerSignedUp" have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName <firstName> and lastName <lastName> and birthDate <birthDate> and kind <kind> and assignedroles "" and unassignedroles ""
    When a MemberSignedUp event with firstName "Niek" and lastName "Schokman" and birthDate "2003-01-01" and kind "PLAYER"
    Then I should find one player with firstName "Niek" and lastName "Schokman" and birthDate "2003-01-01" and kind "PLAYER" and roles ""
    And The events "PlayerSignedUp" have been broadcasted with properties firstName "" and lastName "" and birthDate "" and kind "" and roles "" and eventproperties firstName "Niek" and lastName "Schokman" and birthDate "2003-01-01" and kind "PLAYER" and assignedroles "" and unassignedroles ""
    When specify players for name <teamName> and category <category> and description <description>
    Then I should find one team aggregate with 2 events "TeamRegistered,TeamPlayersAllocated" and teamName <teamName> and category <category> and description <description>
    And I should find one team with name <teamName> in category <category> with description <description> and number of players 2
    And The events "TeamPlayersAllocated" have been broadcasted with properties teamName <teamName> and category <category> and description <description> and number of players 0 and number of assigned players 2
    Examples:
      | teamName  | category | description      | firstName | lastName       | birthDate    | kind     |
      | "JO18-1"  | "JO18"   | "SELECTION_TEAM" | "Sofia"   | "van der Berg" | "2005-08-25" | "PLAYER" |
